/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.organizmy;

import wirtualnyswiat_java.Pole;
import wirtualnyswiat_java.Swiat;
import wirtualnyswiat_java.organizmy.rosliny.BarszczSosnowskiego;
import wirtualnyswiat_java.organizmy.rosliny.Guarana;
import wirtualnyswiat_java.organizmy.rosliny.Mlecz;
import wirtualnyswiat_java.organizmy.rosliny.Trawa;
import wirtualnyswiat_java.organizmy.rosliny.WilczeJagody;
import wirtualnyswiat_java.organizmy.zwierzeta.Antylopa;
import wirtualnyswiat_java.organizmy.zwierzeta.Czlowiek;
import wirtualnyswiat_java.organizmy.zwierzeta.Lis;
import wirtualnyswiat_java.organizmy.zwierzeta.Owca;
import wirtualnyswiat_java.organizmy.zwierzeta.Wilk;
import wirtualnyswiat_java.organizmy.zwierzeta.Zolw;

/**
 *
 * @author nemesor-raddus
 */
public class FabrykaOrganizmow {
    public static final class Klucz { private Klucz() {} }
    private static final Klucz klucz = new Klucz();
    
    public void ustawKontekst(Swiat kontekst)
    {
        this.kontekst=kontekst;
    }
    
    public Organizm stworzAntylope(Pole pozycja)
    {
        return new Antylopa(aktualneID++, pozycja, kontekst, klucz);
    }
    public Organizm stworzCzlowieka(Pole pozycja)
    {
        return new Czlowiek(aktualneID++, pozycja, kontekst, klucz);
    }
    public Organizm stworzLisa(Pole pozycja)
    {
        return new Lis(aktualneID++, pozycja, kontekst, klucz);
    }
    public Organizm stworzOwce(Pole pozycja)
    {
        return new Owca(aktualneID++, pozycja, kontekst, klucz);
    }
    public Organizm stworzWilka(Pole pozycja)
    {
        return new Wilk(aktualneID++, pozycja, kontekst, klucz);
    }
    public Organizm stworzZolwia(Pole pozycja)
    {
        return new Zolw(aktualneID++, pozycja, kontekst, klucz);
    }
    
    public Organizm stworzBarszcz(Pole pozycja)
    {
        return new BarszczSosnowskiego(aktualneID++, pozycja, kontekst, klucz);
    }
    public Organizm stworzGuarane(Pole pozycja)
    {
        return new Guarana(aktualneID++, pozycja, kontekst, klucz);
    }
    public Organizm stworzMlecz(Pole pozycja)
    {
        return new Mlecz(aktualneID++, pozycja, kontekst, klucz);
    }
    public Organizm stworzTrawe(Pole pozycja)
    {
        return new Trawa(aktualneID++, pozycja, kontekst, klucz);
    }
    public Organizm stworzWilczeJagody(Pole pozycja)
    {
        return new WilczeJagody(aktualneID++, pozycja, kontekst, klucz);
    }
    
    public Organizm stworzOrganizm(Pole pozycja, String nazwa, int innaSila)
    {
        Organizm o;
        String nazwa2=nazwa.replace('_', ' ');
        switch (nazwa2)
        {
            case "Antylopa":
                o = stworzAntylope(pozycja);
                break;
            case "Czlowiek":
                o = stworzCzlowieka(pozycja);
                break;
            case "Lis":
                o = stworzLisa(pozycja);
                break;
            case "Owca":
                o = stworzOwce(pozycja);
                break;
            case "Wilk":
                o = stworzWilka(pozycja);
                break;
            case "Zolw":
                o = stworzZolwia(pozycja);
                break;
            case "Barszcz S.":
                o = stworzBarszcz(pozycja);
                break;
            case "Guarana":
                o = stworzGuarane(pozycja);
                break;
            case "Mlecz":
                o = stworzMlecz(pozycja);
                break;
            case "Trawa":
                o = stworzTrawe(pozycja);
                break;
            case "W. jagody":
                o = stworzWilczeJagody(pozycja);
                break;
            default:
                return null;
        }
        if (innaSila != -1)
            o.ustawSila(innaSila);
        return o;
    }
    
    private Swiat kontekst;
    private int aktualneID = 0;
}
