/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.organizmy;

import java.awt.Color;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import wirtualnyswiat_java.Pole;
import wirtualnyswiat_java.Swiat;

/**
 *
 * @author nemesor-raddus
 */
public abstract class Organizm {
    abstract public void wykonajAkcje();
    abstract public void koliduj(Organizm obronca);
    
    public void ustawX(int x)
    {
        pozycja.x = x;
    }
    public void ustawY(int y)
    {
        pozycja.y = y;
    }
    
    public int x()
    {
        return pozycja.x;
    }
    public int y()
    {
        return pozycja.y;
    }
    
    public int inicjatywa()
    {
        return inicjatywa;
    }
    
    public int sila()
    {
        return sila;
    }
    public void ustawSila(int sila)
    {
        this.sila=sila;
    }
    
    public int id()
    {
        return id;
    }
    
    abstract public char litera();
    abstract public Color kolor();
    
    abstract public boolean czyOdbilAtak(Organizm atakujacy);
    public void mozeZostanZabity(Organizm atakujacy)
    {
        zostanZabity();
    }
    public void zostanZabity()
    {
        kontekst.usunOrganizm(this);
    }
    
    public String nazwa()
    {
        return nazwa;
    }
    
    public String dostanDaneDoZapisu()
    {
        StringBuffer r = new StringBuffer();
        r.append(nazwa.replace(' ', '_'));
        r.append(' ');
        r.append(Integer.toString(sila));
        r.append(' ');
        r.append(Integer.toString(pozycja.x));
        r.append(' ');
        r.append(Integer.toString(pozycja.y));
        return new String(r);
    }
    
    static public int litera2Identyfikator(char litera)
    {
        switch (litera)
        {
            case 'A':
                return 0;
            case 'F':
		return 1;
            case 'H':
		return 2;
            case 'S':
		return 3;
            case 'T':
		return 4;
            case 'W':
		return 5;
            case 'B':
		return 6;
            case 'G':
		return 7;
            case 'g':
		return 8;
            case 'h':
		return 9;
            case 's':
		return 10;
            default:
                return -1;
        }
    }
    
    protected Organizm(int id, String nazwa, int inicjatywa, int sila, Pole pozycja, Swiat kontekst)
    {
        this.id=id;
        this.nazwa=nazwa;
        this.inicjatywa=inicjatywa;
        this.sila=sila;
        this.pozycja=pozycja;
        this.kontekst=kontekst;
    }
    
    protected Swiat kontekst()
    {
        return kontekst;
    }
    
    protected LinkedList<Pole> poprawnePolaSasiadujace()
    {
        return kontekst.pobliskiePola(pozycja);
    }
    protected LinkedList<Pole> pustePolaSasiadujace()
    {
        LinkedList<Pole> wszystkie = poprawnePolaSasiadujace();
        LinkedList<Pole> r = new LinkedList<>();
        for (Iterator<Pole> iterator = wszystkie.iterator(); iterator.hasNext();)
        {
            Pole nast = iterator.next();
            if (kontekst.dostanOrganizm(nast) == null)
                r.add(nast);
        }
        return r;
    }
    
    abstract protected Organizm stworzDziecko();
    protected void rozprzestrzeniajSie()
    {
        LinkedList<Pole> pola=pustePolaSasiadujace();
        if (pola.isEmpty())
            return;
        kontekst.dodajOrganizm(stworzDziecko(), pola.get(new Random().nextInt(pola.size())));
        kontekst().dostanLogger().logujRozprzestrzenianie(this);
    }
    
    private final int id;
    private final int inicjatywa;
    private int sila;
    private final Pole pozycja;
    private final Swiat kontekst;
    private final String nazwa;
}
