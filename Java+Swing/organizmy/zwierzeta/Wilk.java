/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.organizmy.zwierzeta;

import wirtualnyswiat_java.Pole;
import wirtualnyswiat_java.Swiat;
import wirtualnyswiat_java.organizmy.FabrykaOrganizmow;
import wirtualnyswiat_java.organizmy.Organizm;

/**
 *
 * @author nemesor-raddus
 */
public class Wilk extends Zwierze {
    @Override
    public final char litera()
    {
        return 'W';
    }
    
    @Override
    public final boolean czyDrapieznik()
    {
        return true;
    }
    
    @Override
    public final Organizm stworzDziecko()
    {
        return kontekst().dostanFabryke().stworzWilka(new Pole(0,0));
    }
    
    public Wilk(int id, Pole pozycja, Swiat kontekst, FabrykaOrganizmow.Klucz klucz)
    {
        super(id, "Wilk", 5, 9, pozycja, kontekst);
    }
}
