/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.organizmy.zwierzeta;

import wirtualnyswiat_java.Pole;
import wirtualnyswiat_java.Swiat;
import wirtualnyswiat_java.organizmy.FabrykaOrganizmow;
import wirtualnyswiat_java.organizmy.Organizm;

/**
 *
 * @author nemesor-raddus
 */
public class Owca extends Zwierze {
    @Override
    public final char litera()
    {
        return 'O';
    }
    
    @Override
    public final boolean czyDrapieznik()
    {
        return false;
    }
    
    @Override
    public final Organizm stworzDziecko()
    {
        return kontekst().dostanFabryke().stworzOwce(new Pole(0,0));
    }
    
    public Owca(int id, Pole pozycja, Swiat kontekst, FabrykaOrganizmow.Klucz klucz)
    {
        super(id, "Owca", 4, 4, pozycja, kontekst);
    }
}
