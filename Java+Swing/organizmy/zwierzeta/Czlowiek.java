/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.organizmy.zwierzeta;

import java.awt.Color;
import wirtualnyswiat_java.Pole;
import wirtualnyswiat_java.Swiat;
import wirtualnyswiat_java.organizmy.FabrykaOrganizmow;
import wirtualnyswiat_java.organizmy.Organizm;

/**
 *
 * @author nemesor-raddus
 */
public class Czlowiek extends Zwierze {
    @Override
    public final void wykonajAkcje()
    {
        if (zakolejkowanyRuch != null)
        {
            poruszSie(zakolejkowanyRuch);
            czyscZakolejkowanyRuch();
        }
        obsluzZdolnoscPoTurze();
    }
    
    @Override
    public final int sila()
    {
        return super.sila() + zdolnoscPozostalyCzasDzialania;
    }
    
    @Override
    public final char litera()
    {
        return 'C';
    }
    @Override
    public final Color kolor()
    {
        return Color.yellow;
    }
    
    public void zakolejkujRuch(Pole miejsce)
    {
        for (Pole pole : poprawnePolaSasiadujace())
            if (pole.x == miejsce.x && pole.y == miejsce.y)
            {
                zakolejkowanyRuch = miejsce;
                break;
            }
    }
    public Pole zakolejkowanyRuch()
    {
        return zakolejkowanyRuch;
    }
    public void czyscZakolejkowanyRuch()
    {
        zakolejkowanyRuch = null;
    }
    
    @Override
    public final boolean czyDrapieznik()
    {
        return true;
    }
    
    public boolean aktywujZdolnosc()
    {
        if (zdolnoscPozostalyCzasOczekiwania > 0)
            return false;
        zdolnoscPozostalyCzasOczekiwania=10;
        zdolnoscPozostalyCzasDzialania=5;
        kontekst().dostanLogger().logujStartUmiejetnosci();
        return true;
    }
    public void obsluzZdolnoscPoTurze()
    {
        if (zdolnoscPozostalyCzasOczekiwania > 0)
        {
            --zdolnoscPozostalyCzasOczekiwania;
            --zdolnoscPozostalyCzasDzialania;
        }
    }
    public boolean czyZdolnoscAktywna()
    {
        return zdolnoscPozostalyCzasDzialania > 0;
    }
    
    @Override
    public final String dostanDaneDoZapisu()
    {
        StringBuffer s = new StringBuffer(super.dostanDaneDoZapisu());
        s.append(' ');
        s.append(Integer.toString(zdolnoscPozostalyCzasOczekiwania)).append(' ');
        s.append(Integer.toString(zdolnoscPozostalyCzasDzialania)).append(' ');
        return new String(s);
    }
    
    public void ustawDaneZPliku(int zdolnoscPozostalyCzasOczekiwania, int zdolnoscPozostalyCzasDzialania)
    {
        this.zdolnoscPozostalyCzasOczekiwania = zdolnoscPozostalyCzasOczekiwania;
        this.zdolnoscPozostalyCzasDzialania = zdolnoscPozostalyCzasDzialania;
    }
    
    @Override
    public final Organizm stworzDziecko()
    {
        return null;
    }
    
    public Czlowiek(int id, Pole pozycja, Swiat kontekst, FabrykaOrganizmow.Klucz klucz)
    {
        super(id, "Czlowiek", 4, 5, pozycja, kontekst);
        zakolejkowanyRuch = null;
        zdolnoscPozostalyCzasOczekiwania = 0;
        zdolnoscPozostalyCzasDzialania = 0;
    }
    
    private Pole zakolejkowanyRuch;
    private int zdolnoscPozostalyCzasOczekiwania, zdolnoscPozostalyCzasDzialania;
}
