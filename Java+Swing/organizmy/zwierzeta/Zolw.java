/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.organizmy.zwierzeta;

import java.util.Random;
import wirtualnyswiat_java.Pole;
import wirtualnyswiat_java.Swiat;
import wirtualnyswiat_java.organizmy.FabrykaOrganizmow;
import wirtualnyswiat_java.organizmy.Organizm;

/**
 *
 * @author nemesor-raddus
 */
public class Zolw extends Zwierze {
    @Override
    public final void wykonajAkcje()
    {
        if (new Random().nextInt() % 4 == 0)
            super.wykonajAkcje();
    }
    
    @Override
    public final char litera()
    {
        return 'Z';
    }
    
    @Override
    public final boolean czyOdbilAtak(Organizm atakujacy)
    {
        return atakujacy.sila() < 5;
    }
    
    @Override
    public final boolean czyDrapieznik()
    {
        return false;
    }
    
    @Override
    public final Organizm stworzDziecko()
    {
        return kontekst().dostanFabryke().stworzZolwia(new Pole(0,0));
    }
    
    public Zolw(int id, Pole pozycja, Swiat kontekst, FabrykaOrganizmow.Klucz klucz)
    {
        super(id, "Zolw", 1, 2, pozycja, kontekst);
    }
}
