/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.organizmy.zwierzeta;

import java.util.Random;
import wirtualnyswiat_java.Pole;
import wirtualnyswiat_java.Swiat;
import wirtualnyswiat_java.organizmy.FabrykaOrganizmow;
import wirtualnyswiat_java.organizmy.Organizm;

/**
 *
 * @author nemesor-raddus
 */
public class Antylopa extends Zwierze {
    @Override
    public final void wykonajAkcje()
    {
        Swiat w = kontekst();
        Organizm t = this;
        super.wykonajAkcje();
        if (w.czyOrganizmZyje(t))
            super.wykonajAkcje();
    }
    
    @Override
    public final char litera()
    {
        return 'A';
    }
    
    @Override
    public final boolean czyMozeUciec()
    {
        return new Random().nextBoolean();
    }
    
    @Override
    public final boolean czyDrapieznik()
    {
        return false;
    }
    
    @Override
    public final Organizm stworzDziecko()
    {
        return kontekst().dostanFabryke().stworzAntylope(new Pole(0,0));
    }
    
    public Antylopa(int id, Pole pozycja, Swiat kontekst, FabrykaOrganizmow.Klucz klucz)
    {
        super(id, "Antylopa", 4, 4, pozycja, kontekst);
    }
}
