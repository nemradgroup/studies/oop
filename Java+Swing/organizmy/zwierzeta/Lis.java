/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.organizmy.zwierzeta;

import java.util.LinkedList;
import java.util.Random;
import wirtualnyswiat_java.Pole;
import wirtualnyswiat_java.Swiat;
import wirtualnyswiat_java.organizmy.FabrykaOrganizmow;
import wirtualnyswiat_java.organizmy.Organizm;

/**
 *
 * @author nemesor-raddus
 */
public class Lis extends Zwierze {
    @Override
    public final void wykonajAkcje()
    {
        LinkedList<Pole> bezpieczne = bezpiecznePola();
        if (!bezpieczne.isEmpty())
            poruszSie(bezpieczne.get(new Random().nextInt(bezpieczne.size())));
    }
    
    @Override
    public final char litera()
    {
        return 'L';
    }
    
    @Override
    public final boolean czyDrapieznik()
    {
        return true;
    }
    
    @Override
    public final Organizm stworzDziecko()
    {
        return kontekst().dostanFabryke().stworzLisa(new Pole(0,0));
    }
    
    public Lis(int id, Pole pozycja, Swiat kontekst, FabrykaOrganizmow.Klucz klucz)
    {
        super(id, "Lis", 7, 3, pozycja, kontekst);
    }
    
    private LinkedList<Pole> bezpiecznePola()
    {
        LinkedList<Pole> poprawne = poprawnePolaSasiadujace();
        LinkedList<Pole> wolne = pustePolaSasiadujace();
        LinkedList<Pole> bezpieczne = new LinkedList<>();
        for (Pole pole : poprawne)
        {
            boolean zawiera=false;
            for (Pole pole1 : wolne) {
                if (pole1.x == pole.x && pole1.y == pole.y)
                {
                    zawiera=true;
                    break;
                }
            }    
            if (zawiera || kontekst().dostanOrganizm(pole).sila() <= sila())
                bezpieczne.add(pole);
        }
        return bezpieczne;
    }
}
