/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.organizmy.zwierzeta;

import java.awt.Color;
import java.util.LinkedList;
import java.util.Random;

import wirtualnyswiat_java.Pole;
import wirtualnyswiat_java.Swiat;
import wirtualnyswiat_java.organizmy.Organizm;

/**
 *
 * @author nemesor-raddus
 */
public abstract class Zwierze extends Organizm {
    @Override
    public void wykonajAkcje()
    {
        poruszSieLosowo();
    }
    @Override
    public void koliduj(Organizm obronca)
    {
        if (obronca instanceof Zwierze && czyTenSamTyp((Zwierze)obronca))
            zabawSie();
        else
            walcz(obronca);
    }
    @Override
    public boolean czyOdbilAtak(Organizm atakujacy)
    {
        return false;
    }
    public boolean czyMozeUciec()
    {
        return false;
    }
    public boolean uciekaj()
    {
        LinkedList<Pole> pola=pustePolaSasiadujace();
        if (pola.isEmpty())
            return false;
        poruszSie(pola.get(new Random().nextInt(pola.size())));
        return true;
    }
    
    public boolean czyTenSamTyp(Zwierze zwierze)
    {
        return getClass() == zwierze.getClass();
    }
    
    public void oznaczJakoMartwe()
    {
        czyPowinnoBycMartwe=true;
    }
    
    @Override
    public Color kolor()
    {
        if (czyDrapieznik())
            return Color.red;
        return Color.cyan;
    }
    
    protected Zwierze(int id, String nazwa, int inicjatywa, int sila, Pole pozycja, Swiat kontekst)
    {
        super(id, nazwa, inicjatywa, sila, pozycja, kontekst);
        czyPowinnoBycMartwe = false;
    }
    
    protected void poruszSie(Pole miejsce)
    {
        try
        {
            Organizm inny = kontekst().dostanOrganizm(miejsce);
            if (inny == null)
                kontekst().przemiescOrganizm(this, miejsce);
            else
                koliduj(inny);
        }
        catch (Throwable t)
        {
            System.err.println("Porusz sie sie rozwalilo");
        }
    }
    
    protected void poruszSieLosowo()
    {
        LinkedList<Pole> dostepne = poprawnePolaSasiadujace();
        if (!dostepne.isEmpty())
            poruszSie(dostepne.get(new Random().nextInt(dostepne.size())));
    }
    
    protected void walcz(Organizm inny)
    {
        try
        {
            if (!inny.czyOdbilAtak(this))
            {
                if (sila() >= inny.sila())
                {
                    Pole miejsce = new Pole(inny.x(), inny.y());
                    if (inny instanceof Zwierze && (((Zwierze)inny).czyMozeUciec() || !czyDrapieznik()))
                    {
                        if (((Zwierze)inny).czyMozeUciec())
                            kontekst().przemiescOrganizm(this, miejsce);
                        kontekst().dostanLogger().logujWalke(this, inny, true, true);
                    }
                    else
                    {
                        kontekst().dostanLogger().logujWalke(this, inny, true, false);
                        inny.mozeZostanZabity(this);
                        kontekst().przemiescOrganizm(this, miejsce);
                    }
                }
                else
                {
                    if (inny instanceof Zwierze && ((Zwierze)inny).czyDrapieznik())
                    {
                        kontekst().dostanLogger().logujWalke(this, inny, false, true);
                        zostanZabity();
                    }
                    else if (!(inny instanceof Zwierze))
                    {
                        kontekst().dostanLogger().logujWalke(this, inny, false, false);
                        inny.zostanZabity();
                        zostanZabity();
                    }
                    else
                        kontekst().dostanLogger().logujWalke(this, inny, true, true);
                }
            }
        }
        catch (Throwable t)
        {
            //<3<3<3<3 happens
        }
    }
    
    protected void zabawSie()
    {
        rozprzestrzeniajSie();
    }
    
    abstract public boolean czyDrapieznik();
    
    private boolean czyPowinnoBycMartwe;
}
