/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.organizmy.rosliny;

import wirtualnyswiat_java.Pole;
import wirtualnyswiat_java.Swiat;
import wirtualnyswiat_java.organizmy.FabrykaOrganizmow;
import wirtualnyswiat_java.organizmy.Organizm;

/**
 *
 * @author nemesor-raddus
 */
public class Guarana extends Roslina {
    @Override
    public final char litera()
    {
        return 'g';
    }
    
    @Override
    public final void mozeZostanZabity(Organizm atakujacy)
    {
        atakujacy.ustawSila(atakujacy.sila()+3);
        zostanZabity();
    }
    
    @Override
    protected final Organizm stworzDziecko()
    {
        return kontekst().dostanFabryke().stworzGuarane(new Pole(0,0));
    }
    
    public Guarana(int id, Pole pozycja, Swiat kontekst, FabrykaOrganizmow.Klucz klucz)
    {
        super(id, "Guarana", 0, pozycja, kontekst);
    }
}
