/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.organizmy.rosliny;

import wirtualnyswiat_java.Pole;
import wirtualnyswiat_java.Swiat;
import wirtualnyswiat_java.organizmy.FabrykaOrganizmow;
import wirtualnyswiat_java.organizmy.Organizm;

/**
 *
 * @author nemesor-raddus
 */
public class Trawa extends Roslina {
    @Override
    public final char litera()
    {
        return 't';
    }
    
    @Override
    protected final Organizm stworzDziecko()
    {
        return kontekst().dostanFabryke().stworzTrawe(new Pole(0,0));
    }
    
    public Trawa(int id, Pole pozycja, Swiat kontekst, FabrykaOrganizmow.Klucz klucz)
    {
        super(id, "Trawa", 0, pozycja, kontekst);
    }
}
