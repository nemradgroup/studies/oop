/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.organizmy.rosliny;

import wirtualnyswiat_java.Pole;
import wirtualnyswiat_java.Swiat;
import wirtualnyswiat_java.organizmy.FabrykaOrganizmow;
import wirtualnyswiat_java.organizmy.Organizm;

/**
 *
 * @author nemesor-raddus
 */
public class WilczeJagody extends Roslina {
    @Override
    public final char litera()
    {
        return 'w';
    }
    
    @Override
    protected final Organizm stworzDziecko()
    {
        return kontekst().dostanFabryke().stworzWilczeJagody(new Pole(0,0));
    }
    
    public WilczeJagody(int id, Pole pozycja, Swiat kontekst, FabrykaOrganizmow.Klucz klucz)
    {
        super(id, "W. jagody", 99, pozycja, kontekst);
    }
}
