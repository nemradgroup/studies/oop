/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.organizmy.rosliny;

import java.util.LinkedList;

import wirtualnyswiat_java.Pole;
import wirtualnyswiat_java.Swiat;
import wirtualnyswiat_java.organizmy.FabrykaOrganizmow;
import wirtualnyswiat_java.organizmy.Organizm;
import wirtualnyswiat_java.organizmy.zwierzeta.Zwierze;

/**
 *
 * @author nemesor-raddus
 */
public class BarszczSosnowskiego extends Roslina {
    @Override
    public final void wykonajAkcje()
    {
        LinkedList<Pole> pola = poprawnePolaSasiadujace();
        for (Pole pole : pola)
            mozeZabijZwierze(pole);
        super.wykonajAkcje();
    }
    
    @Override
    public final char litera()
    {
        return 'b';
    }
    
    @Override
    protected final Organizm stworzDziecko()
    {
        return kontekst().dostanFabryke().stworzBarszcz(new Pole(0,0));
    }
    
    public BarszczSosnowskiego(int id, Pole pozycja, Swiat kontekst, FabrykaOrganizmow.Klucz klucz)
    {
        super(id, "Barszcz S.", 10, pozycja, kontekst);
    }
    
    private void mozeZabijZwierze(Pole punkt)
    {
        if (kontekst().dostanOrganizm(punkt) != null && kontekst().dostanOrganizm(punkt) instanceof Zwierze)
            ((Zwierze)kontekst().dostanOrganizm(punkt)).zostanZabity();
    }
}
