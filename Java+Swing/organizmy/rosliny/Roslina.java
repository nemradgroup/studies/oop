/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.organizmy.rosliny;

import java.awt.Color;
import java.util.Random;
import wirtualnyswiat_java.Pole;
import wirtualnyswiat_java.Swiat;
import wirtualnyswiat_java.organizmy.Organizm;

/**
 *
 * @author nemesor-raddus
 */
public abstract class Roslina extends Organizm {
    @Override
    public void wykonajAkcje()
    {
        if (new Random().nextDouble() <= 0.2)
        {
            rozprzestrzeniajSie();
        }
    }
    @Override
    public void koliduj(Organizm obronca) {}
    
    @Override
    public final int inicjatywa()
    {
        return 0;
    }
    
    @Override
    public final boolean czyOdbilAtak(Organizm atakujacy)
    {
        return false;
    }
    
    @Override
    public Color kolor()
    {
        return Color.green;
    }
    
    protected Roslina(int id, String nazwa, int sila, Pole pozycja, Swiat kontekst)
    {
        super(id, nazwa, 0, sila, pozycja, kontekst);
    }
}
