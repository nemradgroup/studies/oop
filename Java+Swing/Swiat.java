package wirtualnyswiat_java;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.io.PrintStream;

import wirtualnyswiat_java.organizmy.FabrykaOrganizmow;
import wirtualnyswiat_java.organizmy.Organizm;
import wirtualnyswiat_java.organizmy.zwierzeta.Czlowiek;

/**
 *
 * @author nemesor-raddus
 */
public class Swiat {
    private ArrayList<ArrayList <Organizm> > mapa;
    private LinkedList<Organizm> listaOrganizmow;
    private final FabrykaOrganizmow fabryka;
    private final Logger logger;
    private Czlowiek czlowiek;
    private boolean czyHex;

    public Swiat(int szerokosc, int wysokosc, boolean czyHex)
    {
        mapa = new ArrayList<>(szerokosc);
        for (int i=0;i<szerokosc;++i)
        {
            mapa.add(new ArrayList<>(wysokosc));
            for (int j=0;j<wysokosc;++j)
                mapa.get(i).add(null);
        }
        listaOrganizmow = new LinkedList<>();
        this.czyHex = czyHex;
        fabryka = new FabrykaOrganizmow();
        fabryka.ustawKontekst(this);
        logger = new Logger();
    }
    
    public void zmienRozmiar(int hex, int szerokosc, int wysokosc)
    {
        czysc();
        if (hex == 1)
            czyHex=true;
        else
            czyHex=false;
        mapa = new ArrayList<>(szerokosc);
        for (int i=0;i<szerokosc;++i)
        {
            mapa.add(new ArrayList<>(wysokosc));
            for (int j=0;j<wysokosc;++j)
                mapa.get(i).add(null);
        }
        listaOrganizmow = new LinkedList<>();
    }
    public void czysc()
    {
        listaOrganizmow = null;
        mapa = null;
        czlowiek = null;
        logger.czysc();
    }
    
    public void generuj()
    {
        if (szerokosc()<=0 || wysokosc()<=0)
            return;
        
        // Ogolne parametry zapelniania
        double minStartMapaZapelnienie	=			0.3;
	double maxStartMapaZapelnienie =			0.55;

	// Procenty
	double antylopaProcenty =                               0.05;
	double lisProcenty =					0.05;
	double owcaProcenty =           			0.15;
	double zolwProcenty =                                   0.05;
	double wilkProcenty =                                   0.1;

	double wilczeJagodyProcenty =                           0.05;
	double trawaProcenty =                          	0.25;
	double guaranaProcenty =				0.1;
	double barszczSosnowskiegoProcenty =                    0.05;
	double mleczProcenty =                             	0.1;
	// wyzej wymienione procenty powinny byc lacznie rowne 1 dla najlepszych efektow
	// czlowiek jest pomijany tutaj i dodawany automatycznie

	// Aktualne parametry zapelniania
	double losoweZapelnienieMapy = new Random().nextDouble()*(maxStartMapaZapelnienie-minStartMapaZapelnienie)+minStartMapaZapelnienie;
	int calkowitaIloscPol = szerokosc()* wysokosc();
	double aktualneZapelnienieMapy = 0;
	double aktualneMaxZapelnienieMapy = 0; // wykorzystywane do dodawania organizmow tego samego typu dopoki ilosc aktualna nie osiagnie maxa

        // Lista z wolnymi polami
        ArrayList<Pole> wolnePola = new ArrayList<>(szerokosc()*wysokosc());
        for (int i=0;i<szerokosc();++i)
            for (int j=0;j<wysokosc();++j)
                wolnePola.add(new Pole(i, j));

        Pole czPoz = dostanNowePole(wolnePola);
        dodajOrganizm(fabryka.stworzCzlowieka(czPoz), czPoz);
        czlowiek = ((Czlowiek)mapa.get(czPoz.x).get(czPoz.y));
        
        aktualneMaxZapelnienieMapy += antylopaProcenty;
        aktualneZapelnienieMapy = stworzOrganizmyDanegoTypu(wolnePola, calkowitaIloscPol, losoweZapelnienieMapy, aktualneZapelnienieMapy, aktualneMaxZapelnienieMapy, "Antylopa");
        
        aktualneMaxZapelnienieMapy += lisProcenty;
        aktualneZapelnienieMapy = stworzOrganizmyDanegoTypu(wolnePola, calkowitaIloscPol, losoweZapelnienieMapy, aktualneZapelnienieMapy, aktualneMaxZapelnienieMapy, "Lis");
        
        aktualneMaxZapelnienieMapy += owcaProcenty;
        aktualneZapelnienieMapy = stworzOrganizmyDanegoTypu(wolnePola, calkowitaIloscPol, losoweZapelnienieMapy, aktualneZapelnienieMapy, aktualneMaxZapelnienieMapy, "Owca");
        
        aktualneMaxZapelnienieMapy += zolwProcenty;
        aktualneZapelnienieMapy = stworzOrganizmyDanegoTypu(wolnePola, calkowitaIloscPol, losoweZapelnienieMapy, aktualneZapelnienieMapy, aktualneMaxZapelnienieMapy, "Zolw");
        
        aktualneMaxZapelnienieMapy += wilkProcenty;
        aktualneZapelnienieMapy = stworzOrganizmyDanegoTypu(wolnePola, calkowitaIloscPol, losoweZapelnienieMapy, aktualneZapelnienieMapy, aktualneMaxZapelnienieMapy, "Wilk");
        
        aktualneMaxZapelnienieMapy += wilczeJagodyProcenty;
        aktualneZapelnienieMapy = stworzOrganizmyDanegoTypu(wolnePola, calkowitaIloscPol, losoweZapelnienieMapy, aktualneZapelnienieMapy, aktualneMaxZapelnienieMapy, "W. jagody");
        
        aktualneMaxZapelnienieMapy += trawaProcenty;
        aktualneZapelnienieMapy = stworzOrganizmyDanegoTypu(wolnePola, calkowitaIloscPol, losoweZapelnienieMapy, aktualneZapelnienieMapy, aktualneMaxZapelnienieMapy, "Trawa");
        
        aktualneMaxZapelnienieMapy += guaranaProcenty;
        aktualneZapelnienieMapy = stworzOrganizmyDanegoTypu(wolnePola, calkowitaIloscPol, losoweZapelnienieMapy, aktualneZapelnienieMapy, aktualneMaxZapelnienieMapy, "Guarana");
        
        aktualneMaxZapelnienieMapy += barszczSosnowskiegoProcenty;
        aktualneZapelnienieMapy = stworzOrganizmyDanegoTypu(wolnePola, calkowitaIloscPol, losoweZapelnienieMapy, aktualneZapelnienieMapy, aktualneMaxZapelnienieMapy, "Barszcz S.");
        
        aktualneMaxZapelnienieMapy += mleczProcenty;
        stworzOrganizmyDanegoTypu(wolnePola, calkowitaIloscPol, losoweZapelnienieMapy, aktualneZapelnienieMapy, aktualneMaxZapelnienieMapy, "Mlecz");
        
    }
    // Zwroc losowe wolne pole i usun je z listy
    private Pole dostanNowePole(ArrayList<Pole> wolnePola)
    {
        if (wolnePola.isEmpty())
            return new Pole(-1,-1);
        int rindex = new Random().nextInt(wolnePola.size());
        Pole r = wolnePola.get(rindex);
        wolnePola.remove(rindex);
        return r;
    }
    // Stworz wybrany organizm i ustaw go na mapie i liscie
    private void stworzOrganizm(ArrayList<Pole> wolnePola, String nazwa)
    {
        Pole poz = dostanNowePole(wolnePola);
        if (poz.x == -1)
            return;
        dodajOrganizm(fabryka.stworzOrganizm(poz, nazwa, -1), poz);
    }
    private double stworzOrganizmyDanegoTypu(ArrayList<Pole> wolnePola, int iloscPol, double zapelnienieMapy, double zapelnienieAktualne, double zapelnienieMax, String nazwa)
    {
        double d = 1 / zapelnienieMapy / ((double)iloscPol);
        while (zapelnienieAktualne + d < zapelnienieMax)
        {
            stworzOrganizm(wolnePola, nazwa);
            zapelnienieAktualne += d;
        }
        return zapelnienieAktualne;
    }
    
    public void wczytajPlik(String sciezka) throws IOException
    {
        Scanner f = null;
        try
        {
            f = new Scanner(new File(sciezka));
            int hex = f.nextInt(), w = f.nextInt(), h = f.nextInt(), s = f.nextInt();
            zmienRozmiar(hex, w, h);
            for (int i=0;i<s;++i)
            {
                String n = f.next();
                int str = f.nextInt();
                Pole p = new Pole(f.nextInt(), f.nextInt());
                Organizm org = fabryka.stworzOrganizm(p, n, str);
                listaOrganizmow.add(org);
                mapa.get(p.x).set(p.y, org);
                if (org instanceof Czlowiek)
                {
                    czlowiek = (Czlowiek)org;
                    int ac = f.nextInt(), adl = f.nextInt();
                    czlowiek.ustawDaneZPliku(ac, adl);
                }
            }
            f.close();
        }
        finally
        {
            if (f != null)
                f.close();
        }
    }
    public void zapiszPlik(String sciezka) throws IOException
    {
        PrintStream f = null;
        try
        {
            f = new PrintStream(sciezka);
            f.print(czyHexowy() ? 1 : 0);
            f.print(' ');
            f.print(szerokosc());
            f.print(' ');
            f.print(wysokosc());
            f.print(' ');
            f.print(listaOrganizmow.size());
            f.println();
            for (int i=0;i<listaOrganizmow.size();++i)
            {
                f.print(listaOrganizmow.get(i).dostanDaneDoZapisu());
                f.println();
            }
            f.close();
        }
        finally
        {
            if (f != null)
                f.close();
        }
    }
    
    public void wykonajTure()
    {
        for (int i=0;i<listaOrganizmow.size();)
        {
            Organizm org = listaOrganizmow.get(i);
            org.wykonajAkcje();
            if (i>=listaOrganizmow.size())
                break;
            if (listaOrganizmow.get(i) == org)
                ++i;
        }
    }
    
    public int szerokosc()
    {
        return mapa.size();
    }
    public int wysokosc()
    {
        return mapa.isEmpty() ? 0 : mapa.get(0).size();
    }
    
    public LinkedList <Pole> pobliskiePola(Pole pole)
    {
        LinkedList<Pole> list = new LinkedList<>();
        if (czyHex)
        {
            if (pole.x > 0)
                list.add(new Pole(pole.x-1, pole.y));
            if (pole.x < szerokosc()-1)
                list.add(new Pole(pole.x+1, pole.y));
            if (pole.y > 0)
            {
                list.add(new Pole(pole.x, pole.y-1));
                if (pole.x < szerokosc()-1)
                    list.add(new Pole(pole.x+1, pole.y-1));
            }
            if (pole.y < wysokosc()-1)
            {
                list.add(new Pole(pole.x, pole.y+1));
                if (pole.x > 0)
                    list.add(new Pole(pole.x-1, pole.y+1));
            }
        }
        else
        {
            if (pole.x > 0)
                list.add(new Pole(pole.x-1, pole.y));
            if (pole.x < szerokosc()-1)
                list.add(new Pole(pole.x+1, pole.y));
            if (pole.y > 0)
                list.add(new Pole(pole.x, pole.y-1));
            if (pole.y < wysokosc()-1)
                list.add(new Pole(pole.x, pole.y+1));
        }
        return list;
    }
    
    public void dodajOrganizm(Organizm organizm, Pole miejsce)
    {
        mapa.get(miejsce.x).set(miejsce.y, organizm);
        organizm.ustawX(miejsce.x);
        organizm.ustawY(miejsce.y);
        boolean ok=false;
        for (int index=0;index<listaOrganizmow.size();++index)
        {
            Organizm inny=listaOrganizmow.get(index);
            if (inny.inicjatywa() < organizm.inicjatywa() || (inny.inicjatywa() == organizm.inicjatywa() && inny.id() > organizm.id()))
            {
                listaOrganizmow.add(index, organizm);
                ok=true;
                break;
            }
        }
        if (!ok)
            listaOrganizmow.add(organizm);
    }
    
    public void przemiescOrganizm(Organizm organizm, Pole miejsce) throws Throwable
    {
        if (mapa.get(miejsce.x).get(miejsce.y) != null || organizm == null)
            throw new Throwable("Ups");
        int ox = organizm.x(), oy = organizm.y();
        int nx = miejsce.x, ny = miejsce.y;
        mapa.get(nx).set(ny, organizm);
        mapa.get(ox).set(oy, null);
        organizm.ustawX(nx);
        organizm.ustawY(ny);
    }
    
    public void usunOrganizm(Organizm organizm)
    {
        if (organizm == (Organizm)czlowiek)
            czlowiek = null;
        for (int i=0;i<listaOrganizmow.size();++i)
            if (listaOrganizmow.get(i) == organizm)
            {
                listaOrganizmow.remove(i);
                break;
            }
        mapa.get(organizm.x()).set(organizm.y(), null);
    }
    
    public Czlowiek czlowiek()
    {
        return czlowiek;
    }
    
    public Organizm dostanOrganizm(Pole miejsce)
    {
        return mapa.get(miejsce.x).get(miejsce.y);
    }
    public boolean czyOrganizmZyje(Organizm organizm)
    {
        for (Organizm o : listaOrganizmow)
            if (o == organizm)
                return true;
        return false;
    }
    
    public FabrykaOrganizmow dostanFabryke()
    {
        return fabryka;
    }
    
    public Logger dostanLogger()
    {
        return logger;
    }
    
    public boolean czyHexowy()
    {
        return czyHex;
    }
    
    public void ogarnijRuch(Pole delta)
    {
        Pole p=new Pole(czlowiek.x()+delta.x, czlowiek.y()+delta.y);
        if (p.x >= 0 && p.y >= 0 && p.x < szerokosc() && p.y < wysokosc())
            czlowiek.zakolejkujRuch(p);
        wykonajTure();
    }
    public void ogarnijUmiejetnosc()
    {
        czlowiek.aktywujZdolnosc();
    }
}
