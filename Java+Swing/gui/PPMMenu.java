package wirtualnyswiat_java.gui;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import wirtualnyswiat_java.gui.PlanszaPanel;

public class PPMMenu extends MouseAdapter {
    
    private final PlanszaPanel pp;
    
    public PPMMenu(PlanszaPanel pp)
    {
        this.pp=pp;
    }
    
    @Override
    public void mouseClicked(MouseEvent event)
    {
        if(event.getButton()==MouseEvent.BUTTON3)
        {
            Point p=pp.odnajdzPole(event.getX(),event.getY());
            if (p.x == -1)
                return;
            PPMPojawiacz menu=new PPMPojawiacz(p,pp);
            menu.show(event.getComponent(),event.getX(),event.getY());
        }
    }
    
}
