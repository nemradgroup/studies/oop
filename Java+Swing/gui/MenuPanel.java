/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.gui;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

/**
 *
 * @author nemesor-raddus
 */
public class MenuPanel extends JPanel {
    private final JSpinner szerSpinner, wysSpinner;
    private final JCheckBox hexCheckBox;
    private final JLabel szerLabel, wysLabel, hexLabel;
    private final JButton startButton;
    
    public MenuPanel()
    {
        setSize(500, 500);
        szerLabel = new JLabel("Podaj szerokosc");
        szerLabel.setLocation(50, 50);
        add(szerLabel);
        szerSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 99999, 1));
        szerSpinner.setLocation(100,50);
        add(szerSpinner);
        wysLabel = new JLabel("Podaj wysokosc");
        wysLabel.setLocation(50, 100);
        add(wysLabel);
        wysSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 99999, 1));
        wysSpinner.setLocation(100,100);
        add(wysSpinner);
        hexLabel = new JLabel("Czy swiat ma byc szesciokatny?");
        hexLabel.setLocation(50, 150);
        add(hexLabel);
        hexCheckBox = new JCheckBox();
        hexCheckBox.setLocation(100,150);
        add(hexCheckBox);
        startButton = new JButton("Zacznij gre!");
        startButton.setLocation(75, 200);
        startButton.setSize(100,50);
        add(startButton);
        startButton.addActionListener(new AkcjaStart());
    }
    int szerokoscUstawiona()
    {
        return (Integer)szerSpinner.getValue();
    }
    int wysokoscUstawiona()
    {
        return (Integer)wysSpinner.getValue();
    }
    boolean hexUstawiony()
    {
        return hexCheckBox.isSelected();
    }
}
