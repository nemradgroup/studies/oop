/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.gui;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import wirtualnyswiat_java.Swiat;


/**
 *
 * @author nemesor-raddus
 */
public class SluchaczNowegoStworzenia implements ActionListener {
    private final Point punkt;
    private final PlanszaPanel pp;
    private final String nazwa;
    
    public SluchaczNowegoStworzenia(Point punkt, PlanszaPanel pp, String nazwa)
    {
        this.punkt=punkt;
        this.pp=pp;
        this.nazwa=nazwa;
    }
    
    @Override
    public void actionPerformed(ActionEvent ev)
    {
        Swiat s=pp.dostanSwiat();
        wirtualnyswiat_java.Pole pole=new wirtualnyswiat_java.Pole(punkt.x, punkt.y);
        if (s.dostanOrganizm(pole) == null)
            s.dodajOrganizm(s.dostanFabryke().stworzOrganizm(pole, nazwa, -1), pole);
        pp.aktualizuj();
    }
}
