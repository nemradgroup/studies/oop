/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.gui;

import java.awt.Point;
import java.awt.Polygon;

/**
 *
 * @author nemesor-raddus
 */
public class PoleHexowe extends Pole {
    public PoleHexowe(Point pozycja, int rozmiar)
    {
        super(pozycja, rozmiar);
    }
    
    @Override
    public Polygon zrobWielokat()
    {
        Polygon p=new Polygon();
        double s3=Math.sqrt(3);
        p.addPoint(x(), y()+rozmiar()/4);
        p.addPoint((int) (x()+s3*rozmiar()/4), y());
        p.addPoint((int) (x()+s3*rozmiar()/2), y()+rozmiar()/4);
        p.addPoint((int) (x()+s3*rozmiar()/2), y()+rozmiar()*3/4);
        p.addPoint((int) (x()+s3*rozmiar()/4), y()+rozmiar());
        p.addPoint(x(), y()+rozmiar()*3/4);
        p.addPoint(x(), y()+rozmiar()/4);
        return p;
    }
}
