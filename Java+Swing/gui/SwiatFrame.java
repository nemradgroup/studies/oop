package wirtualnyswiat_java.gui;

import java.awt.FlowLayout;
import java.io.IOException;
import javax.swing.JFrame;

import wirtualnyswiat_java.Swiat;

/**
 *
 * @author nemesor-raddus
 */
public class SwiatFrame extends JFrame {
    private SwiatPanel sp;
    private final MenuPanel mp;
    private Swiat swiat;
    
    public SwiatFrame()
    {
        super("Wirtualny świat 171841");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(300,300);
        setResizable(false);
        setLayout(new FlowLayout());
        
        Sluchacz sluchacz = new Sluchacz();
        addKeyListener(sluchacz);

        mp = new MenuPanel();
        mp.setVisible(true);
        add(mp);
        pack();
        setVisible(true);
        AkcjaStart.ustawFrame(this);
        requestFocusInWindow();
    }
    
    public MenuPanel dostanMenu()
    {
        return mp;
    }
    public Swiat dostanSwiat()
    {
        return swiat;
    }
    public void wczytaj()
    {
        try
        {
            remove(sp);
            swiat.wczytajPlik("save.txt");
            zrobNowySwiatPanel(swiat);
        }
        catch (IOException e)
        {
            
        }
    }
    public void zapisz()
    {
        try
        {
            swiat.zapiszPlik("save.txt");
        }
        catch (IOException e)
        {
            
        }
    }
    void zacznijGre(int szer, int wys, boolean hex)
    {
        mp.setVisible(false);
        
        swiat = new Swiat(szer, wys, hex);
        swiat.generuj();
        zrobNowySwiatPanel(swiat);
    }
    public void aktualizuj()
    {
        sp.aktualizuj();repaint();
        requestFocusInWindow();
    }
    
    private void zrobNowySwiatPanel(Swiat swiat)
    {
        sp = new SwiatPanel(swiat);
        add(sp);
        pack();
        repaint();
        requestFocusInWindow();
    }
}
