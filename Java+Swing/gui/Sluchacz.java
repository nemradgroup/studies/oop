/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.gui;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author nemesor-raddus
 */
public class Sluchacz implements KeyListener {
    @Override
    public void keyPressed(KeyEvent ev)
    {
        SwiatFrame rodzic = (SwiatFrame)ev.getSource();
        switch (ev.getKeyCode())
        {
            case KeyEvent.VK_W:
                rodzic.dostanSwiat().ogarnijRuch(new wirtualnyswiat_java.Pole(0,-1));
                break;
            case KeyEvent.VK_A:
                rodzic.dostanSwiat().ogarnijRuch(new wirtualnyswiat_java.Pole(-1,0));
                break;
            case KeyEvent.VK_S:
                rodzic.dostanSwiat().ogarnijRuch(new wirtualnyswiat_java.Pole(0,1));
                break;
            case KeyEvent.VK_D:
                rodzic.dostanSwiat().ogarnijRuch(new wirtualnyswiat_java.Pole(1,0));
                break;
            case KeyEvent.VK_E:
                if (rodzic.dostanSwiat().czyHexowy())
                    rodzic.dostanSwiat().ogarnijRuch(new wirtualnyswiat_java.Pole(1,-1));
                break;
            case KeyEvent.VK_Z:
                if (rodzic.dostanSwiat().czyHexowy())
                    rodzic.dostanSwiat().ogarnijRuch(new wirtualnyswiat_java.Pole(-1,1));
                break;
            case KeyEvent.VK_U:
                rodzic.dostanSwiat().ogarnijUmiejetnosc();
                break;
            case KeyEvent.VK_F5:
                rodzic.zapisz();
                break;
            case KeyEvent.VK_F9:
                rodzic.wczytaj();
                break;
        }
        rodzic.aktualizuj();
    }
    @Override
    public void keyReleased(KeyEvent ev) {}
    @Override
    public void keyTyped(KeyEvent ev) {}
}
