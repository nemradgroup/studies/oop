/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.gui;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 *
 * @author nemesor-raddus
 */
public class AkcjaStart extends AbstractAction {
    private static SwiatFrame frame;
    
    static public void ustawFrame(SwiatFrame frame)
    {
        AkcjaStart.frame=frame;
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        frame.zacznijGre(frame.dostanMenu().szerokoscUstawiona(), frame.dostanMenu().wysokoscUstawiona(), frame.dostanMenu().hexUstawiony());
    }
}
