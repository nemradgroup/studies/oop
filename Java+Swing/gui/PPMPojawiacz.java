package wirtualnyswiat_java.gui;

import java.awt.Point;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;


public class PPMPojawiacz extends JPopupMenu {
    
    private final int menuLength;
    private final JMenuItem[] menuItems;
    
    public PPMPojawiacz(Point punkt, PlanszaPanel pp)
    {
        this.menuLength = 10;
        
        menuItems = new JMenuItem[menuLength];
        menuItems[0] = new JMenuItem("Antylopa");
        menuItems[1] = new JMenuItem("Barszcz S.");
        menuItems[2] = new JMenuItem("Guarana");
        menuItems[3] = new JMenuItem("Lis");
        menuItems[4] = new JMenuItem("Mlecz");
        menuItems[5] = new JMenuItem("Owca");
        menuItems[6] = new JMenuItem("Trawa");
        menuItems[7] = new JMenuItem("W. Jagody");
        menuItems[8] = new JMenuItem("Wilk");
        menuItems[9] = new JMenuItem("Zolw");
        
        for(int i=0;i<menuLength;i++)
        {
            add(menuItems[i]);
            menuItems[i].addActionListener(new SluchaczNowegoStworzenia(punkt, pp, menuItems[i].getText()));
        }
    }
}
