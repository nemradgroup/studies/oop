/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.gui;

import java.awt.Point;
import java.awt.Polygon;

/**
 *
 * @author nemesor-raddus
 */
public class PoleKwadratowe extends Pole {
    public PoleKwadratowe(Point pozycja, int rozmiar)
    {
        super(pozycja, rozmiar);
    }
    
    @Override
    public Polygon zrobWielokat()
    {
        Polygon p=new Polygon();
        p.addPoint(x(), y());
        p.addPoint(x()+rozmiar(), y());
        p.addPoint(x()+rozmiar(), y()+rozmiar());
        p.addPoint(x(), y()+rozmiar());
        p.addPoint(x(), y());
        return p;
    }
}
