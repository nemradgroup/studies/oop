/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.gui;

import java.awt.Color;
import java.awt.Point;
import java.awt.Polygon;

/**
 *
 * @author nemesor-raddus
 */
abstract public class Pole {
    private Color kolor;
    private String nazwa;
    private final Point pozycja;
    private final int rozmiar;
    
    protected Pole(Point pozycja, int rozmiar)
    {
        this.pozycja=pozycja;
        this.rozmiar=rozmiar;
        this.kolor=Color.white;
        this.nazwa=new String();
    }
    
    public int x()
    {
        return pozycja.x;
    }
    public int y()
    {
        return pozycja.y;
    }
    public int rozmiar()
    {
        return rozmiar;
    }
    public Color kolor()
    {
        return kolor;
    }
    public String nazwa()
    {
        return nazwa;
    }
    
    /**
     *
     * @param kolor
     */
    public void ustawKolor(Color kolor)
    {
        this.kolor=kolor;
    }
    
    /**
     *
     * @param nazwa
     */
    public void ustawNazwe(String nazwa)
    {
        this.nazwa=nazwa;
    }
    
    abstract protected Polygon zrobWielokat();
    
}
