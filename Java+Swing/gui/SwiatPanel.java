package wirtualnyswiat_java.gui;

import java.awt.*;
import java.util.LinkedList;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import wirtualnyswiat_java.Swiat;

/**
 *
 * @author nemesor-raddus
 */
public class SwiatPanel extends JPanel {
    private final Swiat swiat;
    private final PlanszaPanel plansza;
    private final JTextArea logiOkienko;
    private final JScrollPane logiOkienkoScroll;
    
    public SwiatPanel(Swiat swiat)
    {
        this.swiat=swiat;
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        plansza = new PlanszaPanel(swiat);
        add(plansza);
        logiOkienko=new JTextArea();
        logiOkienko.setEditable(false);
        logiOkienko.setLineWrap(true);
        logiOkienko.setWrapStyleWord(true);
        logiOkienko.setFocusable(false);
        logiOkienkoScroll=new JScrollPane(logiOkienko);
        logiOkienkoScroll.setFocusable(false);
        logiOkienkoScroll.setPreferredSize(new Dimension(400,200));
        add(logiOkienkoScroll);
    }
    @Override
    protected void paintComponent(Graphics gr)
    {
        super.paintComponent(gr);
    }
    public void aktualizuj()
    {
        plansza.aktualizuj();
        LinkedList<String> logi=swiat.dostanLogger().dostanWszystkieLogi();
        logiOkienko.setText(String.join("\n", logi));
    }
}
