/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.util.ArrayList;
import javax.swing.JPanel;
import wirtualnyswiat_java.Swiat;

/**
 *
 * @author nemesor-raddus
 */
public class PlanszaPanel extends JPanel {
    private final Swiat swiat;
    private final ArrayList<ArrayList<Pole>> tablica;
    private final PPMMenu ppm;
    private final int x0, y0;
    private final int wielkoscPola;
    
    public PlanszaPanel(Swiat swiat)
    {
        this.swiat = swiat;
        wielkoscPola=65;
        setPreferredSize(new Dimension(wielkoscPola*swiat.szerokosc()+(swiat.wysokosc()-1)*wielkoscPola/2+100,wielkoscPola*swiat.wysokosc()+100));
        tablica = new ArrayList<>(swiat.szerokosc());
        x0=10;
        y0=10;
        double s3=Math.sqrt(3);
        for (int i=0;i<swiat.szerokosc();++i)
        {
            tablica.add(new ArrayList<>(swiat.wysokosc()));
            for (int j=0;j<swiat.wysokosc();++j)
            {
                if (swiat.czyHexowy())
                    tablica.get(i).add(new PoleHexowe(new Point((int) (x0+s3*wielkoscPola*i/2+s3*wielkoscPola*j/4), y0+wielkoscPola*3*j/4), wielkoscPola));
                else
                    tablica.get(i).add(new PoleKwadratowe(new Point(x0+i*wielkoscPola, y0+j*wielkoscPola), wielkoscPola));
            }
        }
        ppm=new PPMMenu(this);
        addMouseListener(ppm);
        aktualizuj();
    }
    
    public void ustawPole(Point poz)
    {
        if (swiat.dostanOrganizm(new wirtualnyswiat_java.Pole(poz.x, poz.y)) == null)
        {
            tablica.get(poz.x).get(poz.y).ustawNazwe(new String());
            tablica.get(poz.x).get(poz.y).ustawKolor(Color.white);
        }
        else
        {
            tablica.get(poz.x).get(poz.y).ustawNazwe(swiat.dostanOrganizm(new wirtualnyswiat_java.Pole(poz.x, poz.y)).nazwa());
            tablica.get(poz.x).get(poz.y).ustawKolor(swiat.dostanOrganizm(new wirtualnyswiat_java.Pole(poz.x, poz.y)).kolor());
        }
    }
    
    public Point odnajdzPole(int x, int y)
    {
        double s3=Math.sqrt(3);
        if (swiat.czyHexowy())
        {
            for (int i=0;i<swiat.szerokosc();++i)
            {
                for (int j=0;j<swiat.wysokosc();++j)
                {
                    int x1=(int) (x0 + s3*wielkoscPola*i/2+s3*wielkoscPola*j/4);
                    int y1=y0 + wielkoscPola*3*j/4;
                    if (x >= x1 && x < x1 + wielkoscPola && y >= y1 && y < y1 + wielkoscPola)
                    {
                        return new Point(i, j);
                    }
                }
            }
        }
        else
        {
            for (int i=0;i<swiat.szerokosc();++i)
            {
                for (int j=0;j<swiat.wysokosc();++j)
                {
                    int x1=(int) (x0+i*wielkoscPola);
                    int y1=y0+j*wielkoscPola;
                    if (x >= x1 && x < x1 + wielkoscPola && y >= y1 && y < y1 + wielkoscPola)
                    {
                        return new Point(i, j);
                    }
                }
            }
        }
        return new Point(-1, -1);
    }
    
    public Swiat dostanSwiat()
    {
        return swiat;
    }
    
    public void aktualizuj()
    {
        for (int i=0;i<swiat.szerokosc();++i)
            for (int j=0;j<swiat.wysokosc();++j)
                ustawPole(new Point(i,j));
        repaint();
    }
    
    @Override
    protected void paintComponent(Graphics gr)
    {
        super.paintComponent(gr);
        
        for (ArrayList<Pole> arrayList : tablica) {
            for (Pole pole : arrayList) {
                Polygon p=pole.zrobWielokat();
                gr.setColor(pole.kolor());
                gr.fillPolygon(p);
                gr.setColor(Color.black);
                gr.drawPolygon(p);
                gr.drawString(pole.nazwa(), pole.x()+pole.rozmiar()/12, pole.y()+pole.rozmiar()*2/3);
            }
        }
    }
}
