/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wirtualnyswiat_java;

import java.util.LinkedList;

import wirtualnyswiat_java.organizmy.Organizm;
import wirtualnyswiat_java.organizmy.rosliny.Roslina;
import wirtualnyswiat_java.organizmy.zwierzeta.Zwierze;

/**
 *
 * @author nemesor-raddus
 */
public class Logger {
    private LinkedList<String> logi;
    
    public Logger()
    {
        logi=new LinkedList<>();
    }
    
    public void logujWalke(Organizm atakujacy, Organizm obronca, boolean czyAtakujacyZyje, boolean czyObroncaZyje) throws Throwable
    {
        StringBuffer nowy = new StringBuffer();
        
        if (atakujacy instanceof Zwierze)
        {
            nowy.append("Atakujacy na pozycji [");
            nowy.append(Integer.toString(atakujacy.x()));
            nowy.append(", ");
            nowy.append(Integer.toString(atakujacy.y()));
            nowy.append("] typu ");
            nowy.append(atakujacy.nazwa());
            nowy.append(' ');
            if (obronca instanceof Zwierze)
            {
                if (czyAtakujacyZyje && !czyObroncaZyje)
                    nowy.append("zabil");
                else if (czyAtakujacyZyje && czyObroncaZyje)
                {
                    if (((Zwierze)atakujacy).czyDrapieznik())
                        nowy.append("zlamil zabijajac");
                    else
                        nowy.append("zastraszal");
                }
                else if (!czyAtakujacyZyje && czyObroncaZyje)
                    nowy.append("mocno zlamil probujac zabic");
                else
                    throw new Throwable("Cos poszlo nie tak");
            }
            else if (obronca instanceof Roslina)
            {
                if (czyAtakujacyZyje && !czyObroncaZyje)
                    nowy.append("pomyslnie zjadl");
                else if (!czyAtakujacyZyje && !czyObroncaZyje)
                    nowy.append("zjadl jedzac");
                else
                    throw new Throwable("Cos poszlo nie tak");
            }
            else
                throw new Throwable("Cos poszlo nie tak");
            nowy.append(" organizm typu ");
            nowy.append(obronca.nazwa());
            nowy.append(" na pozycji [");
            nowy.append(Integer.toString(obronca.x()));
            nowy.append(", ");
            nowy.append(Integer.toString(obronca.y()));
            nowy.append("].");
            loguj(new String(nowy));
        }
        else
            throw new Throwable("Cos poszlo nie tak");
    }
    public void logujStartUmiejetnosci()
    {
        loguj("Czlowiek skorzystal ze zdolnosci robienia miksturki sily.");
    }
    public void logujRozprzestrzenianie(Organizm reprodukujacySie)
    {
        StringBuffer nowy = new StringBuffer();
        nowy.append("Organizm typu ");
        nowy.append(reprodukujacySie.nazwa());
        nowy.append(" na pozycji [");
        nowy.append(Integer.toString(reprodukujacySie.x()));
        nowy.append(", ");
        nowy.append(Integer.toString(reprodukujacySie.y()));
        nowy.append("] sie wlasnie rozmnozyl.");
        loguj(new String(nowy));
    }
    
    public void loguj(String wiadomosc)
    {
        logi.add(wiadomosc);
    }
    
    public LinkedList<String> dostanNajnowszeLogi(int ilosc)
    {
        LinkedList<String> r = new LinkedList<>();
        int s=ilosc-logi.size();
        for (int i=0;i<s;++i)
            r.add(new String());
        for (int i=logi.size()-1;r.size()<ilosc && i>=0; --i)
            r.add(logi.get(i));
        
        return r;
    }
    public LinkedList<String> dostanWszystkieLogi()
    {
        return logi;
    }
    
    public void czysc()
    {
        logi.clear();
    }
    
    public int iloscLogow()
    {
        return logi.size();
    }
}
