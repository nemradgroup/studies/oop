#pragma once

#include <cstddef>
#include <vector>

#include "utils/point.h"
#include "organism.h"
#include "animals\human.h"

class World
{
public:
	World(size_t width, size_t height);

	void resize(size_t width, size_t height);
	void clear();

	void generate();

	void readFile(const std::string& path);
	void writeToFile(const std::string& path);

	void doNextTurn();
	void draw();

	size_t width() const;
	size_t height() const;

	bool isValidLocation(const Utils::Point& place) const;

	void addOrganism(Organism* organism, const Utils::Point& place);
	inline OrganismFactory& getFactory()
	{
		return m_factory;
	}
	void moveOrganism(Organism* organism, const Utils::Point& place);
	void deleteOrganism(Organism* organism);
	const Organism* getOrganism(size_t x, size_t y) const; // assumes x and y are valid
	Organism* accessOrganism(size_t x, size_t y); // assumes x and y are valid
	bool isOrganismAlive(Organism* organism) const;

	inline Human* human()
	{
		return m_human;
	}
	inline const Human* human() const
	{
		return m_human;
	}

private:
	std::vector <std::vector <Organism*> > m_map;
	std::vector <Organism*> m_organismsList;
	OrganismFactory m_factory;
	Human* m_human;
};
