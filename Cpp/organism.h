#pragma once

#include <cstdlib>
#include <vector>
#include <string>

#include "utils/direction.h"
#include "utils/point.h"

class World;

class Organism
{
public:
	virtual void doAction() = 0;
	virtual void collide(Organism* defender) = 0;
	void draw();

	void setX(int x);
	void setY(int y);
	inline int x() const
	{
		return m_position.x;
	}
	inline int y() const
	{
		return m_position.y;
	}

	inline int initiative() const
	{
		return m_initiative;
	}
	inline virtual int strength() const
	{
		return m_strength;
	}
	void setStrength(int strength);
	inline int id() const // antiage
	{
		return m_id;
	}
	virtual char letter() const = 0;
	virtual int color() const = 0;

	virtual bool canDeflectAttack(Organism* attacker) = 0;
	virtual inline void maybeGetKilled(Organism* attacker)
	{
		(void)attacker;
		getKilled();
	}

	virtual void getKilled();

	inline std::string name() const
	{
		return m_name;
	}
	virtual std::string getOutData() const;

	static int letterToIdentifier(char let);

protected:
	Organism(int id, const std::string& name, int initiative, int strength, const Utils::Point& position, World* context);

	inline World* context()
	{
		return m_context;
	}
	inline const World* context() const
	{
		return m_context;
	}

	std::vector <Utils::Direction> getValidDirections() const; // all fields not outside the map
	std::vector <Utils::Direction> getSafeDirections() const; // all valid not occupied fields

	virtual Organism* createChild() = 0;
	virtual bool maybeSpread(const Utils::Point& point);
	void doSpreading();

private:
	const int m_id;
	const int m_initiative;
	int m_strength;
	Utils::Point m_position;
	World* const m_context;
	std::string m_name;
};

class Plant;

class Animal : public Organism
{
public:
	virtual void doAction() override;
	virtual void collide(Organism* defender) override;

	virtual inline bool canDeflectAttack(Organism* attacker) override
	{
		(void)attacker;
		return 0;
	}
	virtual inline bool canFlee() const
	{
		return 0;
	}
	bool flee();

	virtual bool isSameType(Animal* animal) const = 0;

	inline void markAsDead()
	{
		m_shouldBeDead=1;
	}

	int color() const override;

	virtual bool predator() const = 0;

protected:
	Animal(int id, const std::string& name, int initiative, int strength, const Utils::Point& position, World* context);

	void move(Utils::Direction direction); // assumes it's a valid move
	void moveRandomly(); // random direction

	void fight(Organism* organism);
	void getSome();

	bool m_shouldBeDead;
};

class Plant : public Organism
{
public:
	virtual void doAction() override;
	virtual void collide(Organism*) override {}

	virtual inline int initiative() const final
	{
		return 0;
	}

	virtual inline bool canDeflectAttack(Organism*) final
	{
		return 0;
	}

	int color() const override;

protected:
	Plant(int id, const std::string& name, int strength, const Utils::Point& position, World* context);
};

class OrganismFactory
{
public:
	void setContext(World* context);

	Organism* createAntelope(const Utils::Point& position);
	Organism* createFox(const Utils::Point& position);
	Organism* createHuman(const Utils::Point& position);
	Organism* createSheep(const Utils::Point& position);
	Organism* createTurtle(const Utils::Point& position);
	Organism* createWolf(const Utils::Point& position);

	Organism* createBelladonna(const Utils::Point& position);
	Organism* createGrass(const Utils::Point& position);
	Organism* createGuarana(const Utils::Point& position);
	Organism* createSosnowskysHogweed(const Utils::Point& position);
	Organism* createSowThistle(const Utils::Point& position);

	Organism* createOrganism(int identifier, const Utils::Point& position, int strengthOverride = -1);

private:
	World* m_context = nullptr;
	int m_currentID = 0;
};
