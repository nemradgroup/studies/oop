#pragma once

#include "../organism.h"

class SosnowskysHogweed : public Plant
{
	friend class OrganismFactory;

public:
	void doAction() final;

	char letter() const final;

protected:
	Organism* createChild() final;

private:
	SosnowskysHogweed(int id, const Utils::Point& position, World* context);

	void maybeKillAnimal(const Utils::Point& point);
};
