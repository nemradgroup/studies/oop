#include "sowthistle.h"

#include "world.h"

void SowThistle::doAction()
{
	Plant::doAction();
	Plant::doAction();
	Plant::doAction();
}

char SowThistle::letter() const
{
	return 's';
}

Organism* SowThistle::createChild()
{
	return context()->getFactory().createSowThistle({0,0});
}

SowThistle::SowThistle(int id, const Utils::Point &position, World *context)
	: Plant(id, "Sow Thistle", 0, position, context) {}
