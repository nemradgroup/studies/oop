#include "grass.h"

#include "world.h"

char Grass::letter() const
{
	return 'G';
}

Organism* Grass::createChild()
{
	return context()->getFactory().createGrass({0,0});
}

Grass::Grass(int id, const Utils::Point &position, World *context)
	: Plant(id, "Grass", 0, position, context) {}
