#pragma once

#include "../organism.h"

class Guarana : public Plant
{
	friend class OrganismFactory;

public:
	char letter() const final;

	void maybeGetKilled(Organism* attacker) final;

protected:
	Organism* createChild() final;

private:
	Guarana(int id, const Utils::Point& position, World* context);
};
