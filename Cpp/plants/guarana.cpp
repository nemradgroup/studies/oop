#include "guarana.h"

#include "world.h"

char Guarana::letter() const
{
	return 'g';
}

void Guarana::maybeGetKilled(Organism* attacker)
{
	attacker->setStrength(attacker->strength()+3);
	getKilled();
}

Organism* Guarana::createChild()
{
	return context()->getFactory().createGuarana({0,0});
}

Guarana::Guarana(int id, const Utils::Point &position, World *context)
	: Plant(id, "Guarana", 0, position, context) {}
