#include "sosnowskyshogweed.h"

#include "utils/point.h"
#include "world.h"

void SosnowskysHogweed::doAction()
{
	maybeKillAnimal({x()+1, y()});
	maybeKillAnimal({x()-1, y()});
	maybeKillAnimal({x(), y()+1});
	maybeKillAnimal({x(), y()-1});
	Plant::doAction();
}

char SosnowskysHogweed::letter() const
{
	return 'h';
}

Organism* SosnowskysHogweed::createChild()
{
	return context()->getFactory().createSosnowskysHogweed({0,0});
}

SosnowskysHogweed::SosnowskysHogweed(int id, const Utils::Point &position, World *context)
	: Plant(id, "Sosnowsky's Hogweed", 10, position, context) {}

void SosnowskysHogweed::maybeKillAnimal(const Utils::Point& point)
{
	if (context()->isValidLocation(point))
	{
		auto a = dynamic_cast<Animal*>(context()->accessOrganism(point.x, point.y));
		if (a != nullptr)
			a->getKilled();
	}
}
