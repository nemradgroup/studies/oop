#pragma once

#include "../organism.h"

class SowThistle : public Plant
{
	friend class OrganismFactory;

public:
	void doAction() final;

	char letter() const final;

protected:
	Organism* createChild() final;

private:
	SowThistle(int id, const Utils::Point& position, World* context);
};
