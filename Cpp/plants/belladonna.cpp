#include "belladonna.h"

#include "world.h"

char Belladonna::letter() const
{
	return 'B';
}

Organism* Belladonna::createChild()
{
	return context()->getFactory().createBelladonna({0,0});
}

Belladonna::Belladonna(int id, const Utils::Point &position, World *context)
	: Plant(id, "Belladonna", 99, position, context) {}
