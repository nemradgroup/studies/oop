#pragma once

#include "../organism.h"

class Grass : public Plant
{
	friend class OrganismFactory;

public:
	char letter() const final;

protected:
	Organism* createChild() final;

private:
	Grass(int id, const Utils::Point& position, World* context);
};
