#pragma once

#include "../organism.h"

class Belladonna : public Plant
{
	friend class OrganismFactory;

public:
	char letter() const final;

protected:
	Organism* createChild() final;

private:
	Belladonna(int id, const Utils::Point& position, World* context);
};
