#pragma once

#include <vector>
#include <string>

class Organism;

class Logger
{
public:
	static Logger& getInstance();

	void logFight(const Organism* attacker, const Organism* defender, bool attackerAlive, bool defenderAlive);
	void logAbility();
	void logReproduce(const Organism* reproducer);

	void log(const std::string& msg);

	std::vector <std::string> getLatestLogs(int desiredSize) const;

private:
	Logger() = default;

	std::vector <std::string> m_logs;
};
