#include "organism.h"

#include <iostream>
#include <vector>
#include <Windows.h>

#include "animals/antelope.h"
#include "animals/fox.h"
#include "animals/human.h"
#include "animals/sheep.h"
#include "animals/turtle.h"
#include "animals/wolf.h"
#include "plants/belladonna.h"
#include "plants/grass.h"
#include "plants/guarana.h"
#include "plants/sosnowskyshogweed.h"
#include "plants/sowthistle.h"
#include "logger.h"
#include "world.h"

using std::string;
using std::to_string;

void Organism::draw()
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(handle, color());
	std::cout << letter();
	SetConsoleTextAttribute(handle, 7);
}

void Organism::setX(int x)
{
	m_position.x = x;
}

void Organism::setY(int y)
{
	m_position.y = y;
}

void Organism::setStrength(int strength)
{
	m_strength = strength;
}

void Organism::getKilled()
{
	context()->deleteOrganism(this);
}

std::string Organism::getOutData() const
{
	string r;
	r += letter();
	r += ' ';
	r += to_string(m_strength)+' ';
	r += to_string(m_position.x) + ' ' + to_string(m_position.y);
	return r;
}

int Organism::letterToIdentifier(char let)
{
	switch (let)
	{
	case 'A':
		return 0;
	case 'F':
		return 1;
	case 'H':
		return 2;
	case 'S':
		return 3;
	case 'T':
		return 4;
	case 'W':
		return 5;
	case 'B':
		return 6;
	case 'G':
		return 7;
	case 'g':
		return 8;
	case 'h':
		return 9;
	case 's':
		return 10;
	default:
		return -1;
	}
}

Organism::Organism(int id, const std::string& name, int initiative, int strength, const Utils::Point &position, World *context)
	: m_id(id), m_name(name), m_initiative(initiative), m_strength(strength), m_position(position), m_context(context) {}

std::vector<Utils::Direction> Organism::getValidDirections() const
{
	std::vector <Utils::Direction> dirs;
	dirs.reserve(4);
	if (y() != 0)
		dirs.push_back(Utils::Direction::Up);
	if (x() != context()->width()-1)
		dirs.push_back(Utils::Direction::Right);
	if (y() != context()->height()-1)
		dirs.push_back(Utils::Direction::Down);
	if (x() != 0)
		dirs.push_back(Utils::Direction::Left);
	return dirs;
}

std::vector<Utils::Direction> Organism::getSafeDirections() const
{
	auto dirs = getValidDirections();
	for (auto it = dirs.begin(); it != dirs.end();)
	{
		switch (*it)
		{
		case Utils::Direction::Up:
			if (context()->getOrganism(x(), y()-1) != nullptr)
				it=dirs.erase(it);
			else
				++it;
			break;
		case Utils::Direction::Right:
			if (context()->getOrganism(x()+1, y()) != nullptr)
				it=dirs.erase(it);
			else
				++it;
			break;
		case Utils::Direction::Down:
			if (context()->getOrganism(x(), y()+1) != nullptr)
				it=dirs.erase(it);
			else
				++it;
			break;
		default:
			if (context()->getOrganism(x()-1, y()) != nullptr)
				it=dirs.erase(it);
			else
				++it;
			break;
		}
	}
	return dirs;
}

bool Organism::maybeSpread(const Utils::Point& point)
{
	if (context()->isValidLocation(point) && context()->getOrganism(point.x, point.y) == nullptr)
	{
		context()->addOrganism(createChild(), point);
		return 1;
	}
	return 0;
}

void Organism::doSpreading()
{
	if (maybeSpread({ x() + 1, y() }) ||
		maybeSpread({ x() - 1, y() }) ||
		maybeSpread({ x(), y() + 1 }) ||
		maybeSpread({ x(), y() - 1 }))
		Logger::getInstance().logReproduce(this);
}

void Animal::doAction()
{
	moveRandomly();
}

void Animal::collide(Organism* defender)
{
	if (dynamic_cast<Animal*>(defender)!=nullptr && isSameType(static_cast<Animal*>(defender)))
		getSome();
	else
		fight(defender);
}

bool Animal::flee()
{
	auto d = getSafeDirections();
	if (d.empty())
		return 0;
	move(d[rand() % d.size()]);
	return 1;
}

int Animal::color() const
{
	if (predator())
		return 12; // red
	return 11; // blue
}

Animal::Animal(int id, const std::string& name, int initiative, int strength, const Utils::Point &position, World *context)
	: Organism(id, name, initiative, strength, position, context), m_shouldBeDead(0) {}

void Animal::move(Utils::Direction direction)
{
	Utils::Point p;
	switch (direction)
	{
	case Utils::Direction::Up:
		p = Utils::Point{x(), y()-1};
		break;
	case Utils::Direction::Right:
		p = Utils::Point{x()+1, y()};
		break;
	case Utils::Direction::Down:
		p = Utils::Point{x(), y()+1};
		break;
	case Utils::Direction::Left:
		p = Utils::Point{x()-1, y()};
		break;
	default:
		throw "Wez si� ogarnij!";
	}
	auto other = context()->accessOrganism(p.x,p.y);
	if (other == nullptr)
		context()->moveOrganism(this, p);
	else
		collide(other);
}

void Animal::moveRandomly()
{
	auto dirs = getValidDirections();
	if (!dirs.empty())
		move(dirs[rand()%dirs.size()]);
}

void Animal::fight(Organism* organism)
{
	if (!organism->canDeflectAttack(this))
	{
		auto anim = dynamic_cast<Animal*>(organism);
		if (strength() >= organism->strength())
		{
			auto place = Utils::Point{organism->x(),organism->y()};
			if (anim != nullptr && (anim->canFlee() || !predator()))
			{
				if (anim->flee())
					context()->moveOrganism(this, place);
				Logger::getInstance().logFight(this, organism, 1, 1);
			}
			else
			{
				Logger::getInstance().logFight(this, organism, 1, 0);
				organism->maybeGetKilled(this);
				context()->moveOrganism(this, place);
			}
		}
		else
		{
			if (anim != nullptr && anim->predator())
			{
				Logger::getInstance().logFight(this, organism, 0, 1);
				getKilled();
			}
			else if (anim == nullptr)
			{
				Logger::getInstance().logFight(this, organism, 0, 0);
				organism->getKilled();
				getKilled();
			}
			else
				Logger::getInstance().logFight(this, organism, 1, 1);
		}
	}
}

void Animal::getSome()
{
	doSpreading();
}

void Plant::doAction()
{
	int chance = 25;
	if (chance > rand()%100)
		doSpreading();
}

int Plant::color() const
{
	return 10; // green
}

Plant::Plant(int id, const std::string& name, int strength, const Utils::Point &position, World *context)
	: Organism(id, name, 0, strength, position, context) {}

void OrganismFactory::setContext(World *context)
{
	m_context = context;
}

Organism *OrganismFactory::createAntelope(const Utils::Point &position)
{
	return new Antelope{m_currentID++, position, m_context};
}

Organism *OrganismFactory::createFox(const Utils::Point &position)
{
	return new Fox{m_currentID++, position, m_context};
}

Organism *OrganismFactory::createHuman(const Utils::Point &position)
{
	return new Human{m_currentID++, position, m_context};
}

Organism *OrganismFactory::createSheep(const Utils::Point &position)
{
	return new Sheep{m_currentID++, position, m_context};
}

Organism *OrganismFactory::createTurtle(const Utils::Point &position)
{
	return new Turtle{m_currentID++, position, m_context};
}

Organism *OrganismFactory::createWolf(const Utils::Point &position)
{
	return new Wolf{m_currentID++, position, m_context};
}

Organism *OrganismFactory::createBelladonna(const Utils::Point &position)
{
	return new Belladonna{m_currentID++, position, m_context};
}

Organism *OrganismFactory::createGrass(const Utils::Point &position)
{
	return new Grass{m_currentID++, position, m_context};
}

Organism *OrganismFactory::createGuarana(const Utils::Point &position)
{
	return new Guarana{m_currentID++, position, m_context};
}

Organism *OrganismFactory::createSosnowskysHogweed(const Utils::Point &position)
{
	return new SosnowskysHogweed{m_currentID++, position, m_context};
}

Organism *OrganismFactory::createSowThistle(const Utils::Point &position)
{
	return new SowThistle{m_currentID++, position, m_context};
}

Organism *OrganismFactory::createOrganism(int identifier, const Utils::Point & position, int strengthOverride)
{
	Organism* o = nullptr;
	switch (identifier)
	{
	case 0:
		o = createAntelope(position);
		break;
	case 1:
		o = createFox(position);
		break;
	case 2:
		o = createHuman(position);
		break;
	case 3:
		o = createSheep(position);
		break;
	case 4:
		o = createTurtle(position);
		break;
	case 5:
		o = createWolf(position);
		break;
	case 6:
		o = createBelladonna(position);
		break;
	case 7:
		o = createGrass(position);
		break;
	case 8:
		o = createGuarana(position);
		break;
	case 9:
		o = createSosnowskysHogweed(position);
		break;
	case 10:
		o = createSowThistle(position);
		break;
	}

	if (strengthOverride != -1)
		o->setStrength(strengthOverride);

	return o;
}
