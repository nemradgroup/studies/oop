#include <iostream>
#include <conio.h>
#include <ctime>
#include <cstdlib>

#include "world.h"
#include "logger.h"

using std::cin;
using std::cout;

int main()
{
	srand(time(NULL));
	size_t w, h;
	cout << "Piotr Juszczyk 171841\n\n";
	cout << "Podaj szerokosc: ";
	cin >> w;
	cout << "Podaj wysokosc: ";
	cin >> h;
	World world{ w,h };
	world.generate();
	int c;
	bool toNextTurn, gameOn=1;
	while (true)
	{
		toNextTurn = 0;
		world.draw();
		cout << "\narrow keys - movement\n";
		cout << "A - antelope, F - fox, H - human, S - sheep, T - turtle, W - wolf,\n";
		cout << "B - belladonna, G - grass, g - guarana, h - Sosnowsky's Hogweed, s - sow thistle\n";

		cout << "Logs:\n";
		auto logs = Logger::getInstance().getLatestLogs(12);
		for (const auto& e : logs)
			cout << e << '\n';

		if (!gameOn)
			break;

		if (world.human()->isAbilityActive())
			cout << "Ability active!\n";

		c = _getch();
		if (c == 0 || c == 224)
		{
			toNextTurn = 1;
			c = _getch();
			switch (c)
			{
			case 72:
				world.human()->queueMove(Utils::Direction::Up);
				break;
			case 80:
				world.human()->queueMove(Utils::Direction::Down);
				break;
			case 75:
				world.human()->queueMove(Utils::Direction::Left);
				break;
			case 77:
				world.human()->queueMove(Utils::Direction::Right);
				break;
			}
		}
		else if (c == 'a')
			world.human()->activateAbility();
		else if (c == 's')
			world.writeToFile("save.txt");
		else if (c == 'o')
			world.readFile("save.txt");

		if (toNextTurn)
		{
			world.doNextTurn();
			if (world.human() == 0)
				gameOn = 0;
			else
				world.human()->handleAbilityAfterNewTurn();
		}
		system("cls");
	}
	system("pause");
}
