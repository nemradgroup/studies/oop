#include "wolf.h"

#include "world.h"

char Wolf::letter() const
{
	return 'W';
}

bool Wolf::isSameType(Animal* animal) const
{
	return dynamic_cast<Wolf*>(animal) != nullptr;
}

Organism* Wolf::createChild()
{
	return context()->getFactory().createWolf({0,0});
}

Wolf::Wolf(int id, const Utils::Point &position, World *context)
	: Animal(id, "Wolf", 5, 9, position, context) {}
