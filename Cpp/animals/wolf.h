#pragma once

#include "../organism.h"

class Wolf : public Animal
{
	friend class OrganismFactory;

public:
	char letter() const final;

	bool isSameType(Animal* animal) const final;

	inline bool predator() const final
	{
		return 1;
	}

protected:
	Organism* createChild() final;

private:
	Wolf(int id, const Utils::Point& position, World* context);
};
