#pragma once

#include "../organism.h"

class Sheep : public Animal
{
	friend class OrganismFactory;

public:
	char letter() const final;

	bool isSameType(Animal* animal) const final;

	inline bool predator() const final
	{
		return 0;
	}

protected:
	Organism* createChild() final;

private:
	Sheep(int id, const Utils::Point& position, World* context);
};
