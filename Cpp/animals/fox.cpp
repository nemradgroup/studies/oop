#include "fox.h"

#include "world.h"

void Fox::doAction()
{
	auto dirsValid = getValidDirections();
	auto dirsTooSafe = getSafeDirections();
	for (const auto& e : dirsTooSafe)
		for (auto it = dirsValid.begin(); it != dirsValid.end(); ++it)
			if (e == *it)
			{
				dirsValid.erase(it);
				break;
			}
	for (auto it = dirsValid.begin(); it != dirsValid.end();)
	{
		const Organism* o;
		switch (*it)
		{
		case Utils::Direction::Up:
			o = context()->getOrganism(x(), y() - 1);
			if (o != nullptr && o->strength() > strength())
				it=dirsValid.erase(it);
			else
				++it;
			break;
		case Utils::Direction::Right:
			o = context()->getOrganism(x() + 1, y());
			if (o != nullptr && o->strength() > strength())
				it=dirsValid.erase(it);
			else
				++it;
			break;
		case Utils::Direction::Down:
			o = context()->getOrganism(x(), y() + 1);
			if (o != nullptr && o->strength() > strength())
				it=dirsValid.erase(it);
			else
				++it;
			break;
		default:
			o = context()->getOrganism(x() - 1, y());
			if (o != nullptr && o->strength() > strength())
				it=dirsValid.erase(it);
			else
				++it;
			break;
		}
	}
	if (!dirsValid.empty())
		move(dirsValid[rand()%dirsValid.size()]);
}

char Fox::letter() const
{
	return 'F';
}

bool Fox::isSameType(Animal* animal) const
{
	return dynamic_cast<Fox*>(animal) != nullptr;
}

Organism* Fox::createChild()
{
	return context()->getFactory().createFox({0,0});
}

Fox::Fox(int id, const Utils::Point &position, World *context)
	: Animal(id, "Fox", 7, 3, position, context) {}
