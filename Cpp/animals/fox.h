#pragma once

#include "../organism.h"

class Fox : public Animal
{
	friend class OrganismFactory;

public:
	void doAction() final;
	
	char letter() const final;

	bool isSameType(Animal* animal) const final;

	inline bool predator() const final
	{
		return 1;
	}

protected:
	Organism* createChild() final;

private:
	Fox(int id, const Utils::Point& position, World* context);
};
