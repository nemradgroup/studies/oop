#include "turtle.h"

#include "world.h"

void Turtle::doAction()
{
	if (rand()%4 == 0)
		Animal::doAction();
}

char Turtle::letter() const
{
	return 'T';
}

bool Turtle::canDeflectAttack(Organism* attacker)
{
	return attacker->strength() < 5;
}

bool Turtle::isSameType(Animal* animal) const
{
	return dynamic_cast<Turtle*>(animal) != nullptr;
}

Organism* Turtle::createChild()
{
	return context()->getFactory().createTurtle({0,0});
}

Turtle::Turtle(int id, const Utils::Point &position, World *context)
	: Animal(id, "Turtle", 1, 2, position, context) {}
