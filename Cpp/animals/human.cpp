#include "human.h"

#include "world.h"
#include "logger.h"

using std::string;
using std::to_string;

void Human::doAction()
{
	if (m_queuedMove != Utils::Direction::Null)
	{
		move(m_queuedMove);
		m_queuedMove = Utils::Direction::Null;
	}
}

char Human::letter() const
{
	return 'H';
}

int Human::strength() const
{
	return Organism::strength()+m_abilityDurationLeft;
}

int Human::color() const
{
	return 14; // yellow
}

void Human::queueMove(Utils::Direction direction)
{
	auto d = getValidDirections();
	for (size_t i=0;i<d.size();++i)
		if (d[i] == direction)
		{
			m_queuedMove = direction;
			break;
		}
}

Utils::Direction Human::queuedMove() const
{
	return Utils::Direction();
}

void Human::clearQueuedMove()
{
	m_queuedMove = Utils::Direction::Null;
}

bool Human::activateAbility()
{
	if (m_abilityCooldown > 0)
		return 0;
	m_abilityCooldown=10;
	m_abilityDurationLeft=5;
	Logger::getInstance().logAbility();
	return 1;
}

void Human::handleAbilityAfterNewTurn()
{
	if (m_abilityCooldown > 0)
	{
		--m_abilityCooldown;
		--m_abilityDurationLeft;
	}
}

std::string Human::getOutData() const
{
	string r = Organism::getOutData() + ' ';
	r += to_string(m_abilityCooldown) + ' ';
	r += to_string(m_abilityDurationLeft) + ' ';
	r += to_string(static_cast<int>(m_queuedMove));
	return r;
}

void Human::setDataFromFile(int abilityCd, int abilityDurationLeft, const Utils::Direction& queue)
{
	m_abilityCooldown = abilityCd;
	m_abilityDurationLeft = abilityDurationLeft;
	m_queuedMove = queue;
}

Human::Human(int id, const Utils::Point &position, World *context)
	: Animal(id, "Human", 4, 5, position, context), m_abilityCooldown(0), m_abilityDurationLeft(0), m_queuedMove(Utils::Direction::Null) {}
