#pragma once

#include "../organism.h"

class Human : public Animal
{
	friend class OrganismFactory;

public:
	void doAction() final;
	int strength() const final;

	char letter() const final;
	int color() const final;

	void queueMove(Utils::Direction direction);
	Utils::Direction queuedMove() const;
	void clearQueuedMove();

	inline bool isSameType(Animal*) const final
	{
		return 0;
	}

	inline bool predator() const final
	{
		return 1;
	}

	bool activateAbility(); // if really activated it, return 1
	void handleAbilityAfterNewTurn();
	inline bool isAbilityActive() const
	{
		return m_abilityDurationLeft > 0;
	}

	std::string getOutData() const override;
	void setDataFromFile(int abilityCd, int abilityDurationLeft, const Utils::Direction& queue);

protected:
	inline Organism* createChild() final
	{
		return nullptr;
	}

private:
	Human(int id, const Utils::Point& position, World* context);

	Utils::Direction m_queuedMove;
	int m_abilityCooldown, m_abilityDurationLeft;
};
