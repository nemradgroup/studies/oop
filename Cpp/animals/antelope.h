#pragma once

#include "../organism.h"

class Antelope : public Animal
{
	friend class OrganismFactory;

public:
	void doAction() final;

	char letter() const final;

	inline bool canFlee() const final
	{
		return rand()%2;
	}

	bool isSameType(Animal* animal) const final;

	inline bool predator() const final
	{
		return 0;
	}

protected:
	Organism* createChild() final;

private:
	Antelope(int id, const Utils::Point& position, World* context);
};
