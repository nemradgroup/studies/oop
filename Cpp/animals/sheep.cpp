#include "sheep.h"

#include "world.h"

char Sheep::letter() const
{
	return 'S';
}

bool Sheep::isSameType(Animal* animal) const
{
	return dynamic_cast<Sheep*>(animal) != nullptr;
}

Organism* Sheep::createChild()
{
	return context()->getFactory().createSheep({0,0});
}

Sheep::Sheep(int id, const Utils::Point &position, World *context)
	: Animal(id, "Sheep", 4, 4, position, context) {}
