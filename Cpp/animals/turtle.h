#pragma once

#include "../organism.h"

class Turtle : public Animal
{
	friend class OrganismFactory;

public:
	virtual void doAction() final;

	char letter() const final;

	bool canDeflectAttack(Organism* attacker) final;

	bool isSameType(Animal* animal) const final;

	inline bool predator() const final
	{
		return 0;
	}

protected:
	Organism* createChild() final;

private:
	Turtle(int id, const Utils::Point& position, World* context);
};
