#include "antelope.h"

#include "world.h"

void Antelope::doAction()
{
	World* w = context();
	Organism* t = this;
	Animal::doAction();
	if (w->isOrganismAlive(t))
		Animal::doAction();
}

char Antelope::letter() const
{
	return 'A';
}

bool Antelope::isSameType(Animal* animal) const
{
	return dynamic_cast<Antelope*>(animal) != nullptr;
}

Organism* Antelope::createChild()
{
	return context()->getFactory().createAntelope({0,0});
}

Antelope::Antelope(int id, const Utils::Point &position, World *context)
	: Animal(id, "Antelope", 4, 4, position, context) {}
