#include "world.h"

#include <cstdlib>
#include <iostream>
#include <fstream>

World::World(size_t width, size_t height)
{
	resize(width, height);
	m_factory.setContext(this);
}

void World::resize(size_t width, size_t height)
{
	clear();
	for (size_t i = 0; i<width; ++i)
	{
		std::vector <Organism*> n;
		n.reserve(height);
		for (size_t j = 0; j<height; ++j)
			n.push_back(nullptr);
		m_map.push_back(n);
	}
}

void World::clear()
{
	for (size_t i = 0; i < m_organismsList.size(); ++i)
		delete m_organismsList[i];
	m_organismsList.clear();
	m_map.clear();
	m_human = nullptr;
}

void World::generate()
{
	if (width()<= 0 || height() <= 0)
		return;

	// General fill params
	const float minStartMapFill	=				0.3;
	const float maxStartMapFill =				0.55;

	// Percentages list
	const float antelopePercentage =			0.05;
	const float foxPercentage =					0.05;
	const float sheepPercentage =				0.15;
	const float turtlePercentage =				0.05;
	const float wolfPercentage =				0.1;

	const float belladonnaPercentage =			0.05;
	const float grassPercentage =				0.25;
	const float guaranaPercentage =				0.1;
	const float sosnowskysHogweedPercentage =	0.05;
	const float sowThistlePercentage =			0.1;
	// aforementioned percentages should all equal 1 (100%) for best results
	// human is excluded from the list and is added automatically

	// Current fill params
	float randMapFill = static_cast<float>(rand() % static_cast<int>((maxStartMapFill-minStartMapFill) * 10000) + minStartMapFill*10000) / 10000;
	int totalFieldsAmount = width() * height();
	float currentMapFill = 0;
	float currentMapFillMax = 0; // used to place organisms of some type until currentMapFill reaches currentMapFillMax

	// Vector with free fields
	std::vector <std::pair <size_t, size_t> > freeFields;
	freeFields.reserve(width() * height());
	for (size_t i=0; i<width(); ++i)
		for (size_t j=0; j<height(); ++j)
			freeFields.push_back({i,j});

	// Return a randomized free field and erase it from the vector
	auto obtainFreeField = [&freeFields]()->std::pair<int,int>
	{
		if (freeFields.empty())
			return {-1,-1};
		auto rindex = rand() % freeFields.size();
		auto r = freeFields[rindex];
		freeFields.erase(freeFields.begin() + rindex);
		return r;
	};

	// Randomize human position
	auto humanPos = obtainFreeField();
	Utils::Point hp{ humanPos.first,humanPos.second };
	addOrganism(m_factory.createHuman(hp), hp);
	m_human = static_cast<Human*>(m_map[humanPos.first][humanPos.second]);

	// Make organism using method from Factory and place it on the map
	auto makeOrganism = [this, &obtainFreeField](auto organismCreator)
	{
		auto pos = obtainFreeField();
		if (pos == std::pair<int,int>{-1,-1})
			return;
		Utils::Point p{ pos.first,pos.second };
		addOrganism(organismCreator(p), p);
	};

	auto makeOrganismsOfSomeType = [this, &makeOrganism, totalFieldsAmount, randMapFill, &currentMapFill, &currentMapFillMax](int organismIdentifier)
	{
		float d = 1 / randMapFill / static_cast<float>(totalFieldsAmount);
		while (currentMapFill + d < currentMapFillMax)
		{
			makeOrganism([this, organismIdentifier](const Utils::Point& point) { return m_factory.createOrganism(organismIdentifier, point); });
			currentMapFill += d;
		}
	};

	// Populate the map
	currentMapFillMax += antelopePercentage;
	makeOrganismsOfSomeType(0);

	currentMapFillMax += foxPercentage;
	makeOrganismsOfSomeType(1);

	currentMapFillMax += sheepPercentage;
	makeOrganismsOfSomeType(3);

	currentMapFillMax += turtlePercentage;
	makeOrganismsOfSomeType(4);

	currentMapFillMax += wolfPercentage;
	makeOrganismsOfSomeType(5);


	currentMapFillMax += belladonnaPercentage;
	makeOrganismsOfSomeType(6);

	currentMapFillMax += grassPercentage;
	makeOrganismsOfSomeType(7);

	currentMapFillMax += guaranaPercentage;
	makeOrganismsOfSomeType(8);

	currentMapFillMax += sosnowskysHogweedPercentage;
	makeOrganismsOfSomeType(9);

	currentMapFillMax += sowThistlePercentage;
	makeOrganismsOfSomeType(10);
}

void World::readFile(const std::string& path)
{
	std::fstream f;
	f.open(path, std::fstream::in);
	if (!f.is_open())
		return;
	int w, h, s;
	f >> w >> h >> s;
	resize(w, h);
	m_organismsList.reserve(s);
	for (size_t i = 0; i < s; ++i)
	{
		char c;
		int str;
		Utils::Point p;
		f >> c >> str >> p.x >> p.y;
		auto org = m_factory.createOrganism(Organism::letterToIdentifier(c), p, str);
		m_organismsList.push_back(org);
		m_map[p.x][p.y] = org;
		if (dynamic_cast<Human*>(org) != 0)
		{
			m_human = static_cast<Human*>(org);
			int ac, adl, qm;
			f >> ac >> adl >> qm;
			m_human->setDataFromFile(ac, adl, static_cast<Utils::Direction>(qm));
		}
	}
	f.close();
}

void World::writeToFile(const std::string& path)
{
	std::fstream f;
	f.open(path, std::fstream::out);
	if (!f.is_open())
		return;
	f << width() << ' ' << height() << ' ' << m_organismsList.size() << '\n';
	for (size_t i = 0; i < m_organismsList.size(); ++i)
		f << m_organismsList[i]->getOutData()<<'\n';
	f.close();
}

void World::doNextTurn()
{
	for (size_t i=0;i<m_organismsList.size();++i)
		m_organismsList[i]->doAction();
}

void World::draw()
{
	for (size_t i = 0; i<width()+2; ++i)
		std::cout<<'#';
	std::cout << '\n';
	for (size_t i = 0; i<height(); ++i)
	{
		std::cout<<'#';
		for (size_t j = 0; j<width(); ++j)
		{
			if (m_map[j][i] == nullptr)
				std::cout<<' ';
			else
				m_map[j][i]->draw();
		}
		std::cout<<"#\n";
	}
	for (size_t i = 0; i<width()+2; ++i)
		std::cout<<'#';
}

size_t World::width() const
{
	return m_map.size();
}

size_t World::height() const
{
	if (m_map.empty())
		return 0;
	return m_map[0].size();
}

bool World::isValidLocation(const Utils::Point& place) const
{
	return (place.x >= 0 && place.x < width() && place.y >= 0 && place.y < height());
}

void World::addOrganism(Organism* organism, const Utils::Point& place)
{
	m_map[place.x][place.y] = organism;
	organism->setX(place.x);
	organism->setY(place.y);
	bool ok=0;
	for (int index=0; index<m_organismsList.size(); ++index)
	{
		auto other = m_organismsList[index];
		if (other->initiative() < organism->initiative() || (other->initiative() == organism->initiative() && other->id() > organism->id()))
		{
			m_organismsList.insert(m_organismsList.begin() + index, organism);
			ok=1;
			break;
		}
	}
	if (!ok)
		m_organismsList.push_back(organism);
}

void World::moveOrganism(Organism* organism, const Utils::Point& place)
{
	if (m_map[place.x][place.y] != nullptr)
		throw "WTFFFF";
	int ox = organism->x(), oy = organism->y();
	int nx = place.x, ny = place.y;
	m_map[nx][ny] = organism;
	m_map[ox][oy] = nullptr;
	organism->setX(nx);
	organism->setY(ny);
}

void World::deleteOrganism(Organism* organism)
{
	if (organism == m_human)
		m_human = 0;

	for (size_t i =0; i<m_organismsList.size(); ++i)
		if (m_organismsList[i] == organism)
		{
			m_organismsList.erase(m_organismsList.begin()+i);
			break;
		}
	m_map[organism->x()][organism->y()] = nullptr;
	delete organism;
}

Organism* World::accessOrganism(size_t x, size_t y)
{
	return m_map[x][y];
}

bool World::isOrganismAlive(Organism* organism) const
{
	for (const auto& e : m_organismsList)
		if (e == organism)
			return 1;
	return 0;
}

const Organism* World::getOrganism(size_t x, size_t y) const
{
	return m_map[x][y];
}
