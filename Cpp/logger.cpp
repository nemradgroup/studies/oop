#include "logger.h"

#include "organism.h"

Logger& Logger::getInstance()
{
	static Logger r;
	return r;
}

void Logger::logFight(const Organism* attacker, const Organism* defender, bool attackerAlive, bool defenderAlive)
{
	std::string newLog;

	if (dynamic_cast<const Animal*>(attacker))
	{
		newLog+="Attacker of type ";
		newLog+=attacker->name();
		newLog+=" ";
		if (dynamic_cast<const Animal*>(defender))
		{
			if (attackerAlive && !defenderAlive)
				newLog+="successfully killed";
			else if (attackerAlive && defenderAlive)
			{
				if (static_cast<const Animal*>(attacker)->predator())
					newLog += "failed to kill";
				else
					newLog += "was intimidating to";
			}
			else if (!attackerAlive && defenderAlive)
				newLog+="failed miserably and got killed by";
			else
				throw "Something's wrong";
		}
		else if (dynamic_cast<const Plant*>(defender))
		{
			if (attackerAlive && !defenderAlive)
				newLog+="successfully ate";
			else if (!attackerAlive && !defenderAlive)
				newLog+="died after eating";
			else
				throw "Something's wrong";
		}
		else
			throw "Something's wrong";
		newLog+=" an organism of type ";
		newLog+=defender->name();
		newLog+=".";
		log(newLog);
	}
	else
		throw "Something's wrong";
}

void Logger::logAbility()
{
	log("Human used ability called Felix Felicis.");
}

void Logger::logReproduce(const Organism* reproducer)
{
	log(std::string("Organism of type ") + reproducer->name() + " has just reproduced.");
}

void Logger::log(const std::string& msg)
{
	m_logs.push_back(msg);
}

std::vector<std::string> Logger::getLatestLogs(int desiredSize) const
{
	std::vector<std::string> r;
	int s = desiredSize - m_logs.size();
	for (int i = 0; i < s; ++i)
		r.push_back({});
	for (int i=m_logs.size()-1; r.size()<desiredSize && i>=0; --i)
		r.push_back(m_logs[i]);

	return r;
}
