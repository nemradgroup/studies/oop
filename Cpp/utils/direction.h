#pragma once

namespace Utils
{

enum class Direction : char
{
	Null,
	Up,
	Right,
	Down,
	Left
};

}
