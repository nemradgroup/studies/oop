import tkinter as tk
from matplotlib import path
from math import sqrt

from WirtualnySwiat.Pole import Pole


class Okno(object):

    def __init__(self, swiat):
        self.__swiat = swiat
        self.__root = tk.Tk()
        szerokosc = 80*swiat.szerokosc()
        wysokosc = 80*swiat.wysokosc()
        self.__root.geometry(str(szerokosc)+"x"+str(wysokosc))
        self.__tryb_btn = tk.Button(self.__root, text="Zmien tryb na prostokatny", command=self.zmien_na_prost)
        self.__tryb_btn.pack()
        self.__plansza = tk.Canvas(self.__root, width=szerokosc, height=wysokosc)
        self.__root.bind("<Key>", self.obsluz_klawisz)
        self.__popup_menu = tk.Menu(self.__root, tearoff=0)
        org_nazwy = ["Antylopa", "Barszcz S.", "CyberOwca", "Lis", "Trawa", "Guarana", "Owca", "Mlecz", "Zolw", "Wilk", "Wilcze j."]
        for nazwa in org_nazwy:
            self.__popup_menu.add_command(label=nazwa, command=lambda name=nazwa: self.popup_item_clicked(name))
        self.__plansza.bind("<Button-3>", self.popup)
        self.__koordKlikniecia = Pole(-1, -1)
        self.__rozmiar_pola = 60
        self.__root.title("171841 Juszczyk Piotr Wirtualny Swiat")
        self.__plansza.pack()
        self.rysuj_hex()
        tk.mainloop()

    def czysc_plansze(self):
        self.__plansza.delete("all")

    def zmien_na_prost(self):
        self.czysc_plansze()
        self.__swiat.zmien_rozmiar(self.__swiat.szerokosc(), self.__swiat.wysokosc(), False)
        self.__swiat.generuj()
        self.rysuj_prost()
        self.stworz_plansze_prost()
        self.invertuj_przycisk_trybu()

    def zmien_na_hex(self):
        self.czysc_plansze()
        self.__swiat.zmien_rozmiar(self.__swiat.szerokosc(), self.__swiat.wysokosc(), True)
        self.__swiat.generuj()
        self.rysuj_hex()
        self.stworz_plansze_hex()
        self.invertuj_przycisk_trybu()

    def invertuj_przycisk_trybu(self):
        if self.__tryb_btn["text"] == "Zmien tryb na hexowy":
            self.__tryb_btn["command"] = self.zmien_na_prost
            self.__tryb_btn["text"] = "Zmien tryb na prostokatny"
        else:
            self.__tryb_btn["command"] = self.zmien_na_hex
            self.__tryb_btn["text"] = "Zmien tryb na hexowy"

    def obsluz_klawisz(self, event):
        if event.char == 'w':
            self.__swiat.ogarnij_ruch(Pole(0, -1))
        elif event.char == 'a':
            self.__swiat.ogarnij_ruch(Pole(-1, 0))
        elif event.char == 's':
            self.__swiat.ogarnij_ruch(Pole(0, 1))
        elif event.char == 'd':
            self.__swiat.ogarnij_ruch(Pole(1, 0))
        elif event.char == 'e':
            if self.__swiat.czy_hexowy():
                self.__swiat.ogarnij_ruch(Pole(1, -1))
        elif event.char == 'z':
            if self.__swiat.czy_hexowy():
                self.__swiat.ogarnij_ruch(Pole(-1, 1))
        elif event.char == 'u':
            self.__swiat.ogarnij_umiejetnosc()
        elif event.char == 'i':
            self.wczytaj()
        elif event.char == 'o':
            self.zapisz()

        self.aktualizuj()

    def wczytaj(self):
        self.__swiat.wczytaj_plik("zapis.txt")
        self.aktualizuj()

    def zapisz(self):
        self.__swiat.zapisz_plik("zapis.txt")

    def aktualizuj(self):
        self.czysc_plansze()
        if self.__swiat.czy_hexowy():
            self.stworz_plansze_hex()
        else:
            self.stworz_plansze_prost()

    def stworz_plansze_prost(self):
        self.rysuj_prost()

    def stworz_plansze_hex(self):
        self.rysuj_hex()

    def popup(self, event):
        self.__koordKlikniecia = self.odnajdz_pole(event.x, event.y)
        try:
            self.__popup_menu.tk_popup(event.x_root, event.y_root, 0)
        finally:
            self.__popup_menu.grab_release()

    def popup_item_clicked(self, name):
        self.dodaj_organizm(name, self.__koordKlikniecia)
        self.__koordKlikniecia = Pole(-1, -1)

    def dodaj_organizm(self, name, koord):
        self.__swiat.dodaj_organizm(self.__swiat.dostan_fabryke().stworz_organizm(koord, name, -1), koord)
        self.aktualizuj()

    def rysuj_prost(self):
        for i in range(self.__swiat.szerokosc()):
            for j in range(self.__swiat.wysokosc()):
                org = self.__swiat.dostan_organizm(Pole(i, j))
                x0 = i*self.__rozmiar_pola
                y0 = j*self.__rozmiar_pola
                if org is not None:
                    self.__plansza.create_polygon(self.stworz_prost(x0, y0), outline="black", fill=org.kolor())
                    self.__plansza.create_text(x0+self.__rozmiar_pola//2, y0+self.__rozmiar_pola//2, text=org.nazwa())
                else:
                    self.__plansza.create_polygon(self.stworz_prost(x0, y0), outline="black", fill="white")

    def rysuj_hex(self):
        s3 = sqrt(3)
        for i in range(self.__swiat.szerokosc()):
            for j in range(self.__swiat.wysokosc()):
                org = self.__swiat.dostan_organizm(Pole(i, j))
                x0 = int(i*self.__rozmiar_pola*s3//2+s3*self.__rozmiar_pola*j//4)
                y0 = self.__rozmiar_pola*j*3//4
                if org is not None:
                    self.__plansza.create_polygon(self.stworz_hex(x0, y0), outline="black", fill=org.kolor())
                    self.__plansza.create_text(x0+self.__rozmiar_pola//2, y0+self.__rozmiar_pola//2, text=org.nazwa())
                else:
                    self.__plansza.create_polygon(self.stworz_hex(x0, y0), outline="black", fill="white")

    def stworz_prost(self, x0, y0):
        rozmiar = self.__rozmiar_pola
        wierzcholki = [[x0, y0],
                       [x0+rozmiar, y0],
                       [x0+rozmiar, y0+rozmiar],
                       [x0, y0+rozmiar],
                       [x0, y0]]
        return wierzcholki

    def stworz_hex(self, x0, y0):
        rozmiar = self.__rozmiar_pola
        s3 = sqrt(3)
        wierzcholki = [[x0, y0+rozmiar//4],
                       [x0+s3*rozmiar//4, y0],
                       [x0+s3*rozmiar//2, y0+rozmiar//4],
                       [x0+s3*rozmiar//2, y0+rozmiar*3//4],
                       [x0+s3*rozmiar//4, y0+rozmiar],
                       [x0, y0+rozmiar*3//4],
                       [x0, y0+rozmiar//4]]
        return wierzcholki

    def odnajdz_pole(self, x, y):
        if self.__swiat.czy_hexowy():
            for i in range(self.__swiat.szerokosc()):
                for j in range(self.__swiat.wysokosc()):
                    if self.czy_hex_zawiera_punkt(self.stworz_hex(i*self.__rozmiar_pola*sqrt(3)//2+sqrt(3)*self.__rozmiar_pola*j//4, j*self.__rozmiar_pola*j*3//4), x, y):
                        return Pole(i, j)
        else:
            for i in range(self.__swiat.szerokosc()):
                for j in range(self.__swiat.wysokosc()):
                    if self.czy_prost_zawiera_punkt(i*self.__rozmiar_pola, j*self.__rozmiar_pola, x, y):
                        return Pole(i, j)
        return Pole(-1, -1)

    def czy_prost_zawiera_punkt(self, x0, y0, x, y):
        return x >= x0 and x < x0+self.__rozmiar_pola and y >= y0 and y < y0+self.__rozmiar_pola

    def czy_hex_zawiera_punkt(self, zbior_wiercholkow, x, y):
        return path.Path(zbior_wiercholkow).contains_point([x, y])
