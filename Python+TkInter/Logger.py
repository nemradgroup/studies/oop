from WirtualnySwiat.Organizmy.Rosliny.Roslina import Roslina
from WirtualnySwiat.Organizmy.Zwierzeta.Zwierze import Zwierze


class Logger(object):
    def __init__(self):
        self.logi = []

    def loguj_walke(self, atakujacy, obronca, czyAtakujacyZyje, czyObroncaZyje):
        nowy = ""
        if isinstance(atakujacy, Zwierze):
            nowy += "Atakujacy na pozycji [" + str(atakujacy.x()) + ", "
            nowy += str(atakujacy.y()) + "] typu " + atakujacy.nazwa() + " "
            if isinstance(obronca, Zwierze):
                if czyAtakujacyZyje and not czyObroncaZyje:
                    nowy += "zabil"
                elif czyAtakujacyZyje and czyObroncaZyje:
                    if atakujacy.czy_drapieznik():
                        nowy += "zlamil zabijajac"
                    else:
                        nowy += "zastraszal"
                elif not czyAtakujacyZyje and czyObroncaZyje:
                    nowy += "mocno zlamil probujac zabic"
                else:
                    raise(Exception, "Cos poszlo nie tak")
            elif isinstance(obronca, Roslina):
                if czyAtakujacyZyje and not czyObroncaZyje:
                    nowy += "pomyslnie zjadl"
                elif not czyAtakujacyZyje and not czyObroncaZyje:
                    nowy += "zjadl jedzac"
                else:
                    raise (Exception, "Cos poszlo nie tak")
            else:
                raise (Exception, "Cos poszlo nie tak")
            nowy += " organizm typu " + obronca.nazwa() + " na pozycji ["
            nowy += str(obronca.x()) + ", " + str(obronca.y()) + "]."
            self.loguj(nowy)
        else:
            raise (Exception, "Cos poszlo nie tak")

    def loguj_start_umiejetnosci(self):
        self.loguj("Czlowiek skorzystal ze zdolnosci robienia miksturki sily.")

    def loguj_rozprzestrzenianie(self, reprodukujacySie):
        nowy = ""
        nowy += "Organizm typu " + reprodukujacySie.nazwa() + " na pozycji ["
        nowy += str(reprodukujacySie.x()) + ", " + str(reprodukujacySie.y());
        nowy += "] sie wlasnie rozmnozyl."
        self.loguj(nowy)

    def loguj(self, wiadomosc):
        self.logi += wiadomosc
        print(wiadomosc)

    def dostan_najnowsze_logi(self, ilosc):
        r = []
        s = ilosc - len(self.logi)
        for i in range(0, s):
            r += ""
        for i in range(len(self.logi)-1, -1, -1):
            if len(r) >= ilosc:
                break
            r += self.logi[i]
        return r

    def dostan_wszystkie_logi(self):
        return self.logi

    def czysc(self):
        self.logi.clear()

    def ilosc_logow(self):
        return len(self.logi)
