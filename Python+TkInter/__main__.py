from WirtualnySwiat.GUI.Okno import Okno
from WirtualnySwiat.Swiat import Swiat


def main():
    print("Witaj w wirtualnym swiecie\n")
    sz = int(input("Podaj szerokosc: "))
    wys = int(input("Podaj wysokosc: "))
    swiat = Swiat(sz, wys, True)
    swiat.generuj()
    okno = Okno(swiat)


if __name__ == "__main__":
    main()