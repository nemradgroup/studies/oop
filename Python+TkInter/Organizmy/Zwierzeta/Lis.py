import random

from WirtualnySwiat.Organizmy.Zwierzeta.Zwierze import Zwierze
from WirtualnySwiat.Pole import Pole


class Lis(Zwierze):

    def __init__(self, id, pozycja, kontekst):
        super().__init__(id, "Lis", 7, 3, pozycja, kontekst)

    def wykonaj_akcje(self):
        bezpieczne = self.bezpieczne_pola()
        if len(bezpieczne) > 0:
            self.porusz_sie(bezpieczne[random.randint(0, len(bezpieczne)-1)])

    def litera(self):
        return 'L'

    def czy_drapieznik(self):
        return True

    def stworz_dziecko(self):
        return self.kontekst().dostan_fabryke().stworz_lisa(Pole(0, 0))

    def bezpieczne_pola(self):
        poprawne = self.poprawne_pola_sasiadujace()
        wolne = self.puste_pola_sasiadujace()
        bezpieczne = []
        for pole in poprawne:
            zawiera = False
            for pole1 in wolne:
                if pole1.x == pole.x and pole1.y == pole.y:
                    zawiera = True
                    break
            if zawiera or self.kontekst().dostan_organizm(pole).sila() <= self.sila():
                bezpieczne.append(pole)
        return bezpieczne
