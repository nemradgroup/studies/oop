from abc import ABC, abstractmethod
import random

from WirtualnySwiat.Organizmy.Organizm import Organizm
from WirtualnySwiat.Pole import Pole


class Zwierze(Organizm):

    def __init__(self, id, nazwa, inicjatywa, sila, pozycja, kontekst):
        super().__init__(id, nazwa, inicjatywa, sila, pozycja, kontekst)
        self.__czy_powinno_byc_martwe = False

    def wykonaj_akcje(self):
        self.porusz_sie_losowo()

    def koliduj(self, obronca):
        if isinstance(obronca, Zwierze) and self.czy_ten_sam_typ(obronca):
            self.zabaw_sie()
        else:
            self.walcz(obronca)

    def czy_odbil_atak(self, atakujacy):
        return False

    def czy_moze_uciec(self):
        return False

    def uciekaj(self):
        pola = self.puste_pola_sasiadujace()
        if len(pola) == 0:
            return False
        self.porusz_sie(pola[random.randint(0, len(pola)-1)])
        return True

    def czy_ten_sam_typ(self, zwierze):
        return type(self) is type(zwierze)

    def oznacz_jako_martwe(self):
        self.__czy_powinno_byc_martwe = True

    def kolor(self):
        if self.czy_drapieznik():
            return "red"
        return "cyan"

    def porusz_sie(self, miejsce):
        try:
            inny = self.kontekst().dostan_organizm(miejsce)
            if inny is None:
                self.kontekst().przemiesc_organizm(self, miejsce)
            else:
                self.koliduj(inny)
        except Exception:
            pass

    def porusz_sie_losowo(self):
        dostepne = self.poprawne_pola_sasiadujace()
        if len(dostepne) > 0:
            self.porusz_sie(dostepne[random.randint(0, len(dostepne)-1)])

    def walcz(self, inny):
        try:
            if not inny.czy_odbil_atak(self):
                if self.sila() >= inny.sila():
                    miejsce = Pole(inny.x(), inny.y())
                    if isinstance(inny, Zwierze) and (inny.czy_moze_uciec() or not self.czy_drapieznik()):
                        if inny.czy_moze_uciec():
                            self.kontekst().przemiesc_organizm(self, miejsce)
                        self.kontekst().dostan_logger().loguj_walke(self, inny, True, True)
                    else:
                        self.kontekst().dostan_logger().loguj_walke(self, inny, True, False)
                        inny.moze_zostan_zabity(self)
                        self.kontekst().przemiesc_organizm(self, miejsce)
                else:
                    if isinstance(inny, Zwierze) and inny.czy_drapieznik():
                        self.kontekst().dostan_logger().loguj_walke(self, inny, False, True)
                        self.zostan_zabity()
                    elif not isinstance(inny, Zwierze):
                        self.kontekst().dostan_logger().loguj_walke(self, inny, False, False)
                        inny.zostan_zabity()
                        self.zostan_zabity()
                    else:
                        self.kontekst().dostan_logger().loguj_walke(self, inny, True, True)
        except Exception:
            pass

    def zabaw_sie(self):
        self.rozprzestrzeniaj_sie()

    @abstractmethod
    def czy_drapieznik(self):
        pass
