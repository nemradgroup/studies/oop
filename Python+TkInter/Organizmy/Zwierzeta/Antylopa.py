import random

from WirtualnySwiat.Organizmy.Zwierzeta.Zwierze import Zwierze
from WirtualnySwiat.Pole import Pole


class Antylopa(Zwierze):

    def __init__(self, id, pozycja, kontekst):
        super().__init__(id, "Antylopa", 4, 4, pozycja, kontekst)

    def wykonaj_akcje(self):
        w = self.kontekst()
        t = self
        super().wykonaj_akcje()
        if w.czy_organizm_zyje(t):
            super().wykonaj_akcje()

    def litera(self):
        return 'A'

    def czy_moze_uciec(self):
        return random.getrandbits(1) == 0

    def czy_drapieznik(self):
        return False

    def stworz_dziecko(self):
        return self.kontekst().dostan_fabryke().stworz_antylope(Pole(0, 0))
