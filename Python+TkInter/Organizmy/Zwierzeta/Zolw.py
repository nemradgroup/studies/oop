import random

from WirtualnySwiat.Organizmy.Zwierzeta.Zwierze import Zwierze
from WirtualnySwiat.Pole import Pole


class Zolw(Zwierze):

    def __init__(self, id, pozycja, kontekst):
        super().__init__(id, "Zolw", 1, 2, pozycja, kontekst)

    def wykonaj_akcje(self):
        if random.randint(0,3) == 0:
            super().wykonaj_akcje()

    def litera(self):
        return 'Z'

    def czy_odbil_atak(self, atakujacy):
        return atakujacy.sila() < 5

    def czy_drapieznik(self):
        return False

    def stworz_dziecko(self):
        return self.kontekst().dostan_fabryke().stworz_zolwia(Pole(0, 0))
