from WirtualnySwiat.Organizmy.Zwierzeta.Zwierze import Zwierze
from WirtualnySwiat.Pole import Pole


class Wilk(Zwierze):

    def __init__(self, id, pozycja, kontekst):
        super().__init__(id, "Wilk", 5, 9, pozycja, kontekst)

    def litera(self):
        return 'W'

    def czy_drapieznik(self):
        return True

    def stworz_dziecko(self):
        return self.kontekst().dostan_fabryke().stworz_wilka(Pole(0, 0))
