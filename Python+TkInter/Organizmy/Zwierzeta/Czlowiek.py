from WirtualnySwiat.Organizmy.Zwierzeta.Zwierze import Zwierze


class Czlowiek(Zwierze):

    def __init__(self, id, pozycja, kontekst):
        super().__init__(id, "Czlowiek", 4, 5, pozycja, kontekst)
        self.__zakolejkowany_ruch = None
        self.__zdolnosc_pozostaly_czas_oczekiwania = 0
        self.__zdolnosc_pozostaly_czas_dzialania = 0

    def wykonaj_akcje(self):
        if not self.__zakolejkowany_ruch is None:
            self.porusz_sie(self.__zakolejkowany_ruch)
            self.czysc_zakolejkowany_ruch()
        self.obsluz_zdolnosc_po_turze()

    def sila(self):
        return super().sila() + self.__zdolnosc_pozostaly_czas_dzialania

    def litera(self):
        return 'C'

    def kolor(self):
        return "yellow"

    def zakolejkuj_ruch(self, miejsce):
        pola = self.poprawne_pola_sasiadujace()
        for pole in pola:
            if pole.x == miejsce.x and pole.y == miejsce.y:
                self.__zakolejkowany_ruch = miejsce
                break

    def zakolejkowany_ruch(self):
        return self.__zakolejkowany_ruch

    def czysc_zakolejkowany_ruch(self):
        self.__zakolejkowany_ruch = None

    def czy_drapieznik(self):
        return True

    def aktywuj_zdolnosc(self):
        if self.__zdolnosc_pozostaly_czas_oczekiwania > 0:
            return False
        self.__zdolnosc_pozostaly_czas_oczekiwania = 10
        self.__zdolnosc_pozostaly_czas_dzialania = 5
        self.kontekst().dostan_logger().loguj_start_umiejetnosci()
        return True

    def obsluz_zdolnosc_po_turze(self):
        if self.__zdolnosc_pozostaly_czas_oczekiwania > 0:
            self.__zdolnosc_pozostaly_czas_dzialania -= 1
            self.__zdolnosc_pozostaly_czas_oczekiwania -= 1

    def czy_zdolnosc_aktywna(self):
        return self.__zdolnosc_pozostaly_czas_dzialania > 0

    def dostan_dane_do_zapisu(self):
        s = super().dostan_dane_do_zapisu()
        s += "\n" + str(self.__zdolnosc_pozostaly_czas_oczekiwania)
        s += "\n" + str(self.__zdolnosc_pozostaly_czas_dzialania) # + " "
        return s

    def ustaw_dane_z_pliku(self, zdolnosc_pozostaly_czas_oczekiwania, zdolnosc_pozostaly_czas_dzialania):
        self.__zdolnosc_pozostaly_czas_oczekiwania = zdolnosc_pozostaly_czas_oczekiwania
        self.__zdolnosc_pozostaly_czas_dzialania = zdolnosc_pozostaly_czas_dzialania
