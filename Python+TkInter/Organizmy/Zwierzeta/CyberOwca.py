from WirtualnySwiat.Organizmy.Zwierzeta.Owca import Owca
from WirtualnySwiat.Pole import Pole


class CyberOwca(Owca):

    def __init__(self, id, pozycja, kontekst):
        super().__init__(id, "CyberOwca", 4, 11, pozycja, kontekst)

    def wykonaj_akcje(self):
        poz_barszczu = self.kontekst().znajdz_barszcz(Pole(self.x(), self.y()))
        if poz_barszczu.x == -1:
            super().wykonaj_akcje()
        else:
            if poz_barszczu.y == self.y():
                if poz_barszczu.x < self.x():
                    self.porusz_sie(Pole(self.x() - 1, self.y()))
                else:
                    self.porusz_sie(Pole(self.x() + 1, self.y()))
            elif poz_barszczu.y < self.y():
                if poz_barszczu.x > self.x():
                    if self.kontekst().czy_hexowy():
                        self.porusz_sie(Pole(self.x() + 1, self.y() - 1))
                    else:
                        self.porusz_sie(Pole(self.x() + 1, self.y()))
                elif poz_barszczu.x < self.x():
                    self.porusz_sie(Pole(self.x() - 1, self.y()))
                else:
                    self.porusz_sie(Pole(self.x(), self.y() - 1))
            else:
                if poz_barszczu.x < self.x():
                    if self.kontekst().czy_hexowy():
                        self.porusz_sie(Pole(self.x() - 1, self.y() + 1))
                    else:
                        self.porusz_sie(Pole(self.x() - 1, self.y()))
                elif poz_barszczu.x > self.x():
                    self.porusz_sie(Pole(self.x() + 1, self.y()))
                else:
                    self.porusz_sie(Pole(self.x(), self.y() + 1))

    def litera(self):
        return 'X'

    def czy_drapieznik(self):
        return False

    def stworz_dziecko(self):
        return self.kontekst().dostan_fabryke().stworz_cyberowce(Pole(0, 0))
