from WirtualnySwiat.Organizmy.Zwierzeta.Zwierze import Zwierze
from WirtualnySwiat.Pole import Pole


class Owca(Zwierze):

    def __init__(self, id, nazwa, inicjatywa, sila, pozycja, kontekst):
        if nazwa is None:
            nazwa = "Owca"
        if inicjatywa is None:
            inicjatywa = 4
        if sila is None:
            sila = 4
        super().__init__(id, nazwa, inicjatywa, sila, pozycja, kontekst)

    def litera(self):
        return 'O'

    def czy_drapieznik(self):
        return False

    def stworz_dziecko(self):
        return self.kontekst().dostan_fabryke().stworz_owce(Pole(0, 0))
