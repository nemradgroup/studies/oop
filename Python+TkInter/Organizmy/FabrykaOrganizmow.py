from WirtualnySwiat.Organizmy.Rosliny.BarszczSosnowskiego import BarszczSosnowskiego
from WirtualnySwiat.Organizmy.Rosliny.Guarana import Guarana
from WirtualnySwiat.Organizmy.Rosliny.Mlecz import Mlecz
from WirtualnySwiat.Organizmy.Rosliny.Trawa import Trawa
from WirtualnySwiat.Organizmy.Rosliny.WilczeJagody import WilczeJagody
from WirtualnySwiat.Organizmy.Zwierzeta.Antylopa import Antylopa
from WirtualnySwiat.Organizmy.Zwierzeta.CyberOwca import CyberOwca
from WirtualnySwiat.Organizmy.Zwierzeta.Czlowiek import Czlowiek
from WirtualnySwiat.Organizmy.Zwierzeta.Lis import Lis
from WirtualnySwiat.Organizmy.Zwierzeta.Owca import Owca
from WirtualnySwiat.Organizmy.Zwierzeta.Wilk import Wilk
from WirtualnySwiat.Organizmy.Zwierzeta.Zolw import Zolw


class FabrykaOrganizmow(object):

    def __init__(self):
        self.__aktualneID = 0
        self.__kontekst = None

    def ustaw_kontekst(self, kontekst):
        self.__kontekst = kontekst

    def stworz_antylope(self, pozycja):
        r = Antylopa(self.__aktualneID, pozycja, self.__kontekst)
        self.__aktualneID += 1
        return r

    def stworz_cyber_owce(self, pozycja):
        r = CyberOwca(self.__aktualneID, pozycja, self.__kontekst)
        self.__aktualneID += 1
        return r

    def stworz_czlowieka(self, pozycja):
        r = Czlowiek(self.__aktualneID, pozycja, self.__kontekst)
        self.__aktualneID += 1
        return r

    def stworz_lisa(self, pozycja):
        r = Lis(self.__aktualneID, pozycja, self.__kontekst)
        self.__aktualneID += 1
        return r

    def stworz_owce(self, pozycja):
        r = Owca(self.__aktualneID, None, None, None, pozycja, self.__kontekst)
        self.__aktualneID += 1
        return r

    def stworz_wilka(self, pozycja):
        r = Wilk(self.__aktualneID, pozycja, self.__kontekst)
        self.__aktualneID += 1
        return r

    def stworz_zolwia(self, pozycja):
        r = Zolw(self.__aktualneID, pozycja, self.__kontekst)
        self.__aktualneID += 1
        return r

    def stworz_barszcz(self, pozycja):
        r = BarszczSosnowskiego(self.__aktualneID, pozycja, self.__kontekst)
        self.__aktualneID += 1
        return r

    def stworz_guarane(self, pozycja):
        r = Guarana(self.__aktualneID, pozycja, self.__kontekst)
        self.__aktualneID += 1
        return r

    def stworz_mlecz(self, pozycja):
        r = Mlecz(self.__aktualneID, pozycja, self.__kontekst)
        self.__aktualneID += 1
        return r

    def stworz_trawe(self, pozycja):
        r = Trawa(self.__aktualneID, pozycja, self.__kontekst)
        self.__aktualneID += 1
        return r

    def stworz_wilcze_jagody(self, pozycja):
        r = WilczeJagody(self.__aktualneID, pozycja, self.__kontekst)
        self.__aktualneID += 1
        return r

    def stworz_organizm(self, pozycja, nazwa, inna_sila):
        nazwa2 = nazwa.replace("_", " ")
        o = {
            'Antylopa': self.stworz_antylope(pozycja),
            'Czlowiek': self.stworz_czlowieka(pozycja),
            'Lis': self.stworz_lisa(pozycja),
            'Owca': self.stworz_owce(pozycja),
            'CyberOwca': self.stworz_cyber_owce(pozycja),
            'Wilk': self.stworz_wilka(pozycja),
            'Zolw': self.stworz_zolwia(pozycja),
            'Barszcz S.': self.stworz_barszcz(pozycja),
            'Guarana': self.stworz_guarane(pozycja),
            'Mlecz': self.stworz_mlecz(pozycja),
            'Trawa': self.stworz_trawe(pozycja),
            'W. jagody': self.stworz_wilcze_jagody(pozycja)
        }.get(nazwa2, None)
        if o is None:
            return None
        if inna_sila != -1:
            o.ustaw_sile(inna_sila)
        return o
