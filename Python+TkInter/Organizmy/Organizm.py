from abc import ABC, abstractmethod

import random


class Organizm(ABC):

    def __init__(self, id, nazwa, inicjatywa, sila, pozycja, kontekst):
        self.__id = id
        self.__nazwa = nazwa
        self.__inicjatywa = inicjatywa
        self.__sila = sila
        self.__pozycja = pozycja
        self.__kontekst = kontekst

    @abstractmethod
    def wykonaj_akcje(self):
        pass

    @abstractmethod
    def koliduj(self, obronca):
        pass

    def ustaw_x(self, x):
        self.__pozycja.x = x

    def ustaw_y(self, y):
        self.__pozycja.y = y

    def x(self):
        return self.__pozycja.x

    def y(self):
        return self.__pozycja.y

    def inicjatywa(self):
        return self.__inicjatywa

    def sila(self):
        return self.__sila

    def ustaw_sile(self, sila):
        self.__sila = sila

    def id(self):
        return self.__id

    @abstractmethod
    def litera(self):
        pass

    @abstractmethod
    def kolor(self):
        pass

    @abstractmethod
    def czy_odbil_atak(self, atakujacy):
        pass

    def moze_zostan_zabity(self, atakujacy):
        self.zostan_zabity()

    def zostan_zabity(self):
        self.__kontekst.usun_organizm(self)

    def nazwa(self):
        return self.__nazwa

    def dostan_dane_do_zapisu(self):
        r = ""
        r += self.__nazwa + "\n" + str(self.__sila) + "\n"
        r += str(self.__pozycja.x) + "\n" + str(self.__pozycja.y)
        return r

    def kontekst(self):
        return self.__kontekst

    def poprawne_pola_sasiadujace(self):
        return self.__kontekst.pobliskie_pola(self.__pozycja)

    def puste_pola_sasiadujace(self):
        wszystkie = self.poprawne_pola_sasiadujace()
        r = []
        for pole in wszystkie:
            if self.__kontekst.dostan_organizm(pole) is None:
                r.append(pole)
        return r

    def stworz_dziecko(self):
        pass

    def rozprzestrzeniaj_sie(self):
        pola = self.puste_pola_sasiadujace()
        if len(pola) == 0:
            return
        self.__kontekst.dodaj_organizm(self.stworz_dziecko(), pola[random.randint(0, len(pola)-1)])
        self.__kontekst.dostan_logger().loguj_rozprzestrzenianie(self)

