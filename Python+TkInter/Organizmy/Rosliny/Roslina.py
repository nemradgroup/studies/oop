import random

from WirtualnySwiat.Organizmy.Organizm import Organizm


class Roslina(Organizm):

    def __init__(self, id, nazwa, sila, pozycja, kontekst):
        super().__init__(id, nazwa, 0, sila, pozycja, kontekst)

    def wykonaj_akcje(self):
        if random.randint(0, 100) <= 4:
            self.rozprzestrzeniaj_sie()

    def koliduj(self, obronca):
        pass

    def inicjatywa(self):
        return 0

    def czy_odbil_atak(self, atakujacy):
        return False

    def kolor(self):
        return "green"
