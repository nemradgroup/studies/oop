from WirtualnySwiat.Organizmy.Rosliny.Roslina import Roslina
from WirtualnySwiat.Pole import Pole


class WilczeJagody(Roslina):

    def __init__(self, id, pozycja, kontekst):
        super().__init__(id, "W. jagody", 99, pozycja, kontekst)

    def litera(self):
        return 'w'

    def stworz_dziecko(self):
        return self.kontekst().dostan_fabryke().stworz_wilcze_jagody(Pole(0, 0))
