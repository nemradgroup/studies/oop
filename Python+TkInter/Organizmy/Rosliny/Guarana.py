from WirtualnySwiat.Organizmy.Rosliny.Roslina import Roslina
from WirtualnySwiat.Pole import Pole


class Guarana(Roslina):

    def __init__(self, id, pozycja, kontekst):
        super().__init__(id, "Guarana", 0, pozycja, kontekst)

    def litera(self):
        return 'g'

    def moze_zostan_zabity(self, atakujacy):
        atakujacy.ustaw_sile(atakujacy.sila() + 3)
        super().moze_zostan_zabity(atakujacy)

    def stworz_dziecko(self):
        return self.kontekst().dostan_fabryke().stworz_guarane(Pole(0, 0))
