from WirtualnySwiat.Organizmy.Rosliny.Roslina import Roslina
from WirtualnySwiat.Pole import Pole


class BarszczSosnowskiego(Roslina):

    def __init__(self, id, pozycja, kontekst):
        super().__init__(id, "Barszcz S.", 10, pozycja, kontekst)

    def wykonaj_akcje(self):
        pola = self.poprawne_pola_sasiadujace()
        for pole in pola:
            self.moze_zabij_zwierze(pole)
        super().wykonaj_akcje()

    def litera(self):
        return 'b'

    def stworz_dziecko(self):
        return self.kontekst().dostan_fabryke().stworz_barszcz(Pole(0, 0))

    def moze_zabij_zwierze(self, punkt):
        from WirtualnySwiat.Organizmy.Zwierzeta.Zwierze import Zwierze
        if not self.kontekst().dostan_organizm(punkt) is None and isinstance(self.kontekst().dostan_organizm(punkt), Zwierze):
            self.kontekst().dostan_organizm(punkt).zostan_zabity()
