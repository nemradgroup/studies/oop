from WirtualnySwiat.Organizmy.Rosliny.Roslina import Roslina
from WirtualnySwiat.Pole import Pole


class Trawa(Roslina):

    def __init__(self, id, pozycja, kontekst):
        super().__init__(id, "Trawa", 0, pozycja, kontekst)

    def litera(self):
        return 't'

    def stworz_dziecko(self):
        return self.kontekst().dostan_fabryke().stworz_trawe(Pole(0, 0))
