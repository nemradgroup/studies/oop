from WirtualnySwiat.Organizmy.Rosliny.Roslina import Roslina
from WirtualnySwiat.Pole import Pole


class Mlecz(Roslina):

    def __init__(self, id, pozycja, kontekst):
        super().__init__(id, "Mlecz", 0, pozycja, kontekst)

    def wykonaj_akcje(self):
        super().wykonaj_akcje()
        super().wykonaj_akcje()
        super().wykonaj_akcje()

    def litera(self):
        return 'm'

    def stworz_dziecko(self):
        return self.kontekst().dostan_fabryke().stworz_mlecz(Pole(0, 0))
