import random

from WirtualnySwiat.Logger import Logger
from WirtualnySwiat.Organizmy.FabrykaOrganizmow import FabrykaOrganizmow
from WirtualnySwiat.Organizmy.Zwierzeta.Czlowiek import Czlowiek
from WirtualnySwiat.Organizmy.Rosliny.BarszczSosnowskiego import BarszczSosnowskiego
from WirtualnySwiat.Pole import Pole


class Swiat(object):

    def __init__(self, szerokosc, wysokosc, czy_hex):
        self.__mapa = []
        for i in range(0, szerokosc):
            self.__mapa.append([])
            for j in range(0, wysokosc):
                self.__mapa[i].append(None)
        self.__lista_organizmow = []
        self.__czlowiek = None
        self.__czy_hex = czy_hex
        self.__fabryka = FabrykaOrganizmow()
        self.__fabryka.ustaw_kontekst(self)
        self.__logger = Logger()

    def zmien_rozmiar(self, szerokosc, wysokosc, czy_hex):
        self.czysc()
        if czy_hex == 0:
            self.__czy_hex = False
        else:
            self.__czy_hex = True
        self.__mapa = []
        for i in range(0, szerokosc):
            self.__mapa.append([])
            for j in range(0, wysokosc):
                self.__mapa[i].append(None)
        self.__lista_organizmow = []

    def czysc(self):
        self.__lista_organizmow = None
        self.__mapa = None
        self.__czlowiek = None
        self.__logger.czysc()

    def generuj(self):
        if self.szerokosc() <= 0 or self.wysokosc() <= 0:
            return

        # Ogolne parametry zapelniania
        min_start_mapa_zapelnienie =            0.3
        max_start_mapa_zapelnienie =            0.55

        # Procenty
        antylopa_procenty =                     0.05
        lis_procenty =                          0.05
        owca_procenty =                         0.1
        cyber_owca_procenty =                   0.05
        zolw_procenty =                         0.05
        wilk_procenty =                         0.1

        wilcze_jagody_procenty =                0.05
        trawa_procenty =                        0.25
        guarana_procenty =                      0.1
        barszcz_sosnowskiego_procenty =         0.05
        mlecz_procenty =                        0.1
        # wyzej wymienione procenty powinny byc lacznie rowne 1 dla najlepszych efektow
        # czlowiek jest pomijany tutaj i dodawany automatycznie

        # Aktualne parametry zapelniania
        losowe_zapelnienie_mapy = random.uniform(min_start_mapa_zapelnienie, max_start_mapa_zapelnienie)
        calkowita_ilosc_pol = self.szerokosc() * self.wysokosc()
        aktualne_zapelnienie_mapy = 0
        aktualne_max_zapelnienie_mapy = 0 # wykorzystywane do dodawania organizmow tego samego typu dopoki ilosc aktualna nie osiagnie maxa

        # Lista z wolnymi polami
        wolne_pola = []
        for i in range(0, self.szerokosc()):
            for j in range(0, self.wysokosc()):
                wolne_pola.append(Pole(i, j))

        cz_poz = self.__dostan_nowe_pole(wolne_pola)
        self.dodaj_organizm(self.__fabryka.stworz_czlowieka(cz_poz), cz_poz)
        self.__czlowiek = self.__mapa[cz_poz.x][cz_poz.y]

        aktualne_max_zapelnienie_mapy += antylopa_procenty
        aktualne_zapelnienie_mapy = self.__stworz_organizmy_danego_typu(wolne_pola, calkowita_ilosc_pol, losowe_zapelnienie_mapy, aktualne_zapelnienie_mapy, aktualne_max_zapelnienie_mapy, "Antylopa")

        aktualne_max_zapelnienie_mapy += lis_procenty
        aktualne_zapelnienie_mapy = self.__stworz_organizmy_danego_typu(wolne_pola, calkowita_ilosc_pol, losowe_zapelnienie_mapy, aktualne_zapelnienie_mapy, aktualne_max_zapelnienie_mapy, "Lis")

        aktualne_max_zapelnienie_mapy += owca_procenty
        aktualne_zapelnienie_mapy = self.__stworz_organizmy_danego_typu(wolne_pola, calkowita_ilosc_pol, losowe_zapelnienie_mapy, aktualne_zapelnienie_mapy, aktualne_max_zapelnienie_mapy, "Owca")

        aktualne_max_zapelnienie_mapy += cyber_owca_procenty
        aktualne_zapelnienie_mapy = self.__stworz_organizmy_danego_typu(wolne_pola, calkowita_ilosc_pol, losowe_zapelnienie_mapy, aktualne_zapelnienie_mapy, aktualne_max_zapelnienie_mapy, "CyberOwca")

        aktualne_max_zapelnienie_mapy += zolw_procenty
        aktualne_zapelnienie_mapy = self.__stworz_organizmy_danego_typu(wolne_pola, calkowita_ilosc_pol, losowe_zapelnienie_mapy, aktualne_zapelnienie_mapy, aktualne_max_zapelnienie_mapy, "Zolw")

        aktualne_max_zapelnienie_mapy += wilk_procenty
        aktualne_zapelnienie_mapy = self.__stworz_organizmy_danego_typu(wolne_pola, calkowita_ilosc_pol, losowe_zapelnienie_mapy, aktualne_zapelnienie_mapy, aktualne_max_zapelnienie_mapy, "Wilk")

        aktualne_max_zapelnienie_mapy += wilcze_jagody_procenty
        aktualne_zapelnienie_mapy = self.__stworz_organizmy_danego_typu(wolne_pola, calkowita_ilosc_pol, losowe_zapelnienie_mapy, aktualne_zapelnienie_mapy, aktualne_max_zapelnienie_mapy, "W. jagody")

        aktualne_max_zapelnienie_mapy += trawa_procenty
        aktualne_zapelnienie_mapy = self.__stworz_organizmy_danego_typu(wolne_pola, calkowita_ilosc_pol, losowe_zapelnienie_mapy, aktualne_zapelnienie_mapy, aktualne_max_zapelnienie_mapy, "Trawa")

        aktualne_max_zapelnienie_mapy += guarana_procenty
        aktualne_zapelnienie_mapy = self.__stworz_organizmy_danego_typu(wolne_pola, calkowita_ilosc_pol, losowe_zapelnienie_mapy, aktualne_zapelnienie_mapy, aktualne_max_zapelnienie_mapy, "Guarana")

        aktualne_max_zapelnienie_mapy += barszcz_sosnowskiego_procenty
        aktualne_zapelnienie_mapy = self.__stworz_organizmy_danego_typu(wolne_pola, calkowita_ilosc_pol, losowe_zapelnienie_mapy, aktualne_zapelnienie_mapy, aktualne_max_zapelnienie_mapy, "Barszcz S.")

        aktualne_max_zapelnienie_mapy += mlecz_procenty
        aktualne_zapelnienie_mapy = self.__stworz_organizmy_danego_typu(wolne_pola, calkowita_ilosc_pol, losowe_zapelnienie_mapy, aktualne_zapelnienie_mapy, aktualne_max_zapelnienie_mapy, "Mlecz")

    # Zwroc losowe wolne pole i usun je z listy
    def __dostan_nowe_pole(self, wolne_pola):
        if len(wolne_pola) == 0:
            return Pole(-1, -1)
        rindex = random.randint(0, len(wolne_pola)-1)
        r = wolne_pola[rindex]
        wolne_pola.remove(r)
        return r

    def __stworz_organizm(self, wolne_pola, nazwa):
        poz = self.__dostan_nowe_pole(wolne_pola)
        if poz.x == -1:
            return
        self.dodaj_organizm(self.__fabryka.stworz_organizm(poz, nazwa, -1), poz)

    def __stworz_organizmy_danego_typu(self, wolne_pola, ilosc_pol, zapelnienie_mapy, zapelnienie_aktualne, zapelnienie_max, nazwa):
        d = 1 / zapelnienie_mapy / ilosc_pol
        while zapelnienie_aktualne + d < zapelnienie_max:
            self.__stworz_organizm(wolne_pola, nazwa)
            zapelnienie_aktualne += d
        return zapelnienie_aktualne

    def wczytaj_plik(self, sciezka):
        f = open(sciezka, "r")
        if f.closed:
            return

        hex = int(f.readline())
        w, h, s = int(f.readline()), int(f.readline()), int(f.readline())
        self.zmien_rozmiar(int(w), int(h), int(hex))
        for i in range(0, s):
            n = f.readline().replace("\n","")
            sila = int(f.readline())
            p = Pole(int(f.readline()), int(f.readline()))
            org = self.__fabryka.stworz_organizm(p, n, sila)
            self.__lista_organizmow.append(org)
            self.__mapa[p.x][p.y] = org
            if isinstance(org, Czlowiek):
                self.__czlowiek = org
                ac, adl = int(f.readline()), int(f.readline())
                self.__czlowiek.ustaw_dane_z_pliku(ac, adl)
        f.close()

    def zapisz_plik(self, sciezka):
        f = open(sciezka, "w")
        if f.closed:
            return

        if self.__czy_hex:
            f.write("1\n")
        else:
            f.write("0\n")
        f.write(str(self.szerokosc())+"\n")
        f.write(str(self.wysokosc())+"\n")
        f.write(str(len(self.__lista_organizmow))+"\n")
        for org in self.__lista_organizmow:
            f.write(org.dostan_dane_do_zapisu()+"\n")
        f.close()

    def wykonaj_ture(self):
        if self.__czlowiek is None:
            return
        if len(self.__lista_organizmow) == 0:
            return
        i = 0
        while True:
            org = self.__lista_organizmow[i]
            org.wykonaj_akcje()
            if i >= len(self.__lista_organizmow):
                break
            if self.__lista_organizmow[i] is org:
                i += 1
            if i >= len(self.__lista_organizmow):
                break

    def szerokosc(self):
        return len(self.__mapa)

    def wysokosc(self):
        if len(self.__mapa) == 0:
            return 0
        return len(self.__mapa[0])

    def pobliskie_pola(self, pole):
        lista = []
        if self.__czy_hex:
            if pole.x > 0:
                lista.append(Pole(pole.x-1, pole.y))
            if pole.x < self.szerokosc()-1:
                lista.append(Pole(pole.x+1, pole.y))
            if pole.y > 0:
                lista.append(Pole(pole.x, pole.y-1))
                if pole.x < self.szerokosc()-1:
                    lista.append(Pole(pole.x+1, pole.y-1))
            if pole.y < self.wysokosc()-1:
                lista.append(Pole(pole.x, pole.y+1))
                if pole.x > 0:
                    lista.append(Pole(pole.x-1, pole.y+1))
        else:
            if pole.x > 0:
                lista.append(Pole(pole.x-1, pole.y))
            if pole.x < self.szerokosc()-1:
                lista.append(Pole(pole.x+1, pole.y))
            if pole.y > 0:
                lista.append(Pole(pole.x, pole.y-1))
            if pole.y < self.wysokosc()-1:
                lista.append(Pole(pole.x, pole.y+1))
        return lista

    def dodaj_organizm(self, organizm, miejsce):
        self.__mapa[miejsce.x][miejsce.y] = organizm
        organizm.ustaw_x(miejsce.x)
        organizm.ustaw_y(miejsce.y)
        ok = False
        for i in range(0, len(self.__lista_organizmow)):
            inny = self.__lista_organizmow[i]
            if inny.inicjatywa() < organizm.inicjatywa() or (inny.inicjatywa() == inny.inicjatywa() and inny.id() > organizm.id()):
                self.__lista_organizmow.insert(i, organizm)
                ok = True
                break
        if not ok:
            self.__lista_organizmow.append(organizm)

    def przemiesc_organizm(self, organizm, miejsce):
        if (self.__mapa[miejsce.x][miejsce.y] is not None) or organizm is None:
            raise(Exception, "Ups")
        ox, oy = organizm.x(), organizm.y()
        nx, ny = miejsce.x, miejsce.y
        self.__mapa[nx][ny] = organizm
        self.__mapa[ox][oy] = None
        organizm.ustaw_x(nx)
        organizm.ustaw_y(ny)

    def usun_organizm(self, organizm):
        if organizm is self.__czlowiek:
            self.__czlowiek = None
        x = organizm.x()
        y = organizm.y()
        for i in range(0, len(self.__lista_organizmow)):
            if self.__lista_organizmow[i] is organizm:
                del self.__lista_organizmow[i]
                break
        self.__mapa[x][y] = None

    def czlowiek(self):
        return self.__czlowiek

    def znajdz_barszcz(self, pozycja):
        poz_barszczu = Pole(-1, -1)
        odl_od_barszczu = 999999
        for x in range(self.szerokosc()):
            for y in range(self.wysokosc()):
                if (self.__mapa[x][y] is not None) and isinstance(self.__mapa[x][y], BarszczSosnowskiego):
                    odl = 0
                    absxdiff = abs(x - pozycja.x)
                    absydiff = abs(y - pozycja.y)
                    if (y < pozycja.y and x > pozycja.x) or (y > pozycja.y and x < pozycja.x):
                        odl = min(absxdiff, absydiff)
                        odl += (absxdiff - odl) + (absydiff - odl)
                    else:
                        odl = absxdiff + absydiff
                    if odl < odl_od_barszczu:
                        poz_barszczu = Pole(x, y)
                        odl_od_barszczu = odl
        return poz_barszczu

    def dostan_organizm(self, miejsce):
        return self.__mapa[miejsce.x][miejsce.y]

    def czy_organizm_zyje(self, organizm):
        for org in self.__lista_organizmow:
            if org is organizm:
                return True
        return False

    def dostan_fabryke(self):
        return self.__fabryka

    def dostan_logger(self):
        return self.__logger

    def czy_hexowy(self):
        return self.__czy_hex

    def ogarnij_ruch(self, delta):
        if self.__czlowiek is None:
            return
        p = Pole(self.__czlowiek.x()+delta.x, self.__czlowiek.y()+delta.y)
        if 0 <= p.x < self.szerokosc() and 0 <= p.y < self.wysokosc():
            self.__czlowiek.zakolejkuj_ruch(p)
        self.wykonaj_ture()

    def ogarnij_umiejetnosc(self):
        self.__czlowiek.aktywuj_zdolnosc()

